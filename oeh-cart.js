﻿var cart;

/*
 * Maintains the state of the cart and provides hooks to synchronise changes
 * back to the server.
 */
function CartViewModel() {
    var self = this;
    self.maxDownLoadSize = 4096;
    self.unknownFileSize = 0;

    self.ItemsIgnored = new Array();

    self.Datasets = [];
    
    self.addDataset = function (dataset) {
        if (dataset == null || !(dataset instanceof Dataset)) {
            self.Datasets.push(new Dataset("", "", "", []));
        } else {
            self.Datasets.push(dataset);
        }
    }

    self.clear = function () {
        self.Datasets = [];
    }

    self.fetch = function (idString) {
        $.get('../../Dataset/Fetch', { id: idString }).done(function (data, textStatus, jqXHR) {
            var json = $.parseJSON(data);
            if (json.url != null) {
                window.open(json.url);
            }
        }).error(function (data, textStatus, jqXHR) {
            console.log("Unable to package dataset: " + textStatus);
            showDialog("Unable to package dataset: " + textStatus);
        });
    }

    self.packageAndFetch = function (dataset) {
        var json = dataset.toJson();
        $.post('../../Dataset/PackageDataset', JSON.stringify(json)).done(function (data, textStatus, jqXHR) {
            var json = $.parseJSON(data);
            if (json.url != null) {
                window.open(json.url);
            }
        }).error(function (data, textStatus, jqXHR) {
            console.log("Unable to retrieve dataset: " + textStatus);
            showDialog("Unable to retrieve dataset: " + textStatus);
        });
    }

    self.getSelectedDatasets = function () {
        var selected = [];
        $.each(self.Datasets, function (index, dataset) {
                selected.push(dataset.Id);
        });
        $.each(self.ItemsIgnored, function (index, path) {
            id = path.split("_")[0];
            selected.pop(path);
        });
        return selected;
    }

    self.getDataset = function(id) {
        var result;
        $.each(self.Datasets, function(index, dataset) {
            if(dataset.Id == id) {
                result = dataset;
                return false;
            }
        });
        return result;
    }

    self.package = function () {
        if (self.getDownloadItemsSize() > this.maxDownLoadSize) {
            showDialog("Max download size is " + self.maxDownLoadSize + ". Please uncheck items.");
            return;
        }
        $("#cart-spinner").show();
        var idList = self.getSelectedDatasets();
        $.post('../../Dataset/Package', JSON.stringify(idList)).done(function (data, textStatus, jqXHR) {
            $("#cart-spinner").hide();
            console.log("Package created, beginning download.");
            showDialog("Package created, beginning download.");
            var json = $.parseJSON(data);
            if (json.url != null) {
                window.open(json.url);
            }
            $.each(idList, function (index, Id) {
                var dataset = self.getDataset(Id);
                dataset.remove();
            });
            //getCart();
        }).error(function (data, textStatus, jqXHR) {
            $("#cart-spinner").hide();
            console.log("Unable to package dataset: " + textStatus);
            showDialog("Unable to package dataset: " + textStatus);
        });
    }
    /* 159000 - Sebastian 13/04/2015 Remove functionality */
    self.removeDataset = function () {
        $("#cart-spinner").show();
        var idList = self.getSelectedDatasets();
        $.each(idList, function (index, Id) {
            var dataset = self.getDataset(Id);
            dataset.remove();
        });
        getCart();
    }

    self.addItemToIgnore =  function(path) {
       // if (!$.inArray(path, self.ItemsIgnored) >= 0) {
            self.ItemsIgnored.push(path);
       // }
    }

    self.removeItemToIgnore =  function (path) {
       // if ($.inArray(path, self.ItemsIgnored) >= 0) {
            self.ItemsIgnored.pop(path);
      //  }
    }

    self.getDownloadItemsSize =  function () {
        var downloadSize = 0;
        $.each(self.Datasets, function (index, value) {
            var dataset = value;
            $.each(dataset.DatasetItems, function (index2, value2) {
                var datasetItem = value2;
                var key = dataset.Id + "_" +datasetItem.Path;
                if (!($.inArray(key, cart.ItemsIgnored) > -1)) {
                    downloadSize += datasetItem.Size;
                }
            });
        });
        return downloadSize;
    }

}

/*
 * Represents a single metadata record in the cart.
 */
function Dataset(Id, Title, Icon, DatasetItems) {
    var self = this;
    self.Id = Id;
    self.Title = Title;
    self.Icon = Icon;
    self.DatasetItems = DatasetItems;

    self.addDatasetItem = function () {
        self.DatasetItems.push(new DatasetItem("", ""));
    }

    self.toJson = function () {
        var meJson = {};
        meJson.MetadataId = self.Id;
        meJson.Title = self.Title;
        meJson.Icon = self.Icon;

        var items = [];

        $.each(self.DatasetItems, function (index, item) {
            var json = item.toJson();
            items[index] = json;
        });

        meJson.DatasetItems = items;

        return meJson;
    }

    self.save = function () {
        var json = this.toJson();
        $.post('../../Dataset/Update', JSON.stringify(json)).done(function (data, textStatus, jqXHR) {
            console.log("Successfully saved dataset.");
            showDialog("Successfully saved dataset.");
            getCart();
        }).error(function (data, textStatus, jqXHR) {
            console.log("Unable to update dataset: " + textStatus);
            showDialog("Unable to update dataset: " + textStatus);
        });

        //saveDataset(this);
    }

    self.remove = function () {
        var json = this.toJson();
        $.post('../../Dataset/Delete', JSON.stringify(json)).done(function (data, textStatus, jqXHR) {
            console.log("Successfully removed dataset.");
            getCart();
        }).error(function (data, textStatus, jqXHR) {
            console.log("Unable to delete dataset: " + textStatus);
            showDialog("Unable to delete dataset: " + textStatus);
        });
    }


}

/* 
 * Represents a downloadable dataset for a metadata record.
 *
 * Format: mimetype string of the item's format
 * Path: path to the downloadable item, as listed in the metadata
 * Size: size in MB of the package, as listed in the metadata
 */
function DatasetItem(Format, Path, Size) {
    var self = this;
    self.Format = Format;
    self.Path = Path;
    self.Size = Size;
    

    self.toJson = function () {
        result = {};
        result.Format = self.Format;
        result.Path = self.Path;
        result.Size = self.Size;
        return result;
    }
}

function getCart() {
    if (cart == null) {
        cart = new CartViewModel();
    }
    $.getJSON("../../Dataset/Index", null, function (data, textStatus, jqXHR) {
        cart.clear();
        $.each(data, function (i, datum) {
            items = [];
            $.each(datum.DatasetItems, function (j, stuff) {
                var item = new DatasetItem(stuff.Format, stuff.Path, stuff.Size);
                items.push(item);
            });
            cart.Datasets.push(new Dataset(datum.MetadataId, datum.Title, datum.Icon, items));
        });
        loadCart();
    });
}

function saveDataset(Dataset) {
    json = Dataset.toJson();
    $.ajax({
        url: '../../Dataset/Update',
        data: json,
        processData: false,
        dataType: 'json',
        type: 'POST',
        success: function (data, textStatus, jqXHR) {
            console.log("Success " + textStatus);
        },
        error: function (data, textStatus, jqXHR) {
            console.log("Failure " + textStatus);
        }
    });
}

function loadCart() {
    $(".tab-cart-items").remove();

   $.each(cart.Datasets, function (index, value) {
       var dataset = value;
       $.each(dataset.DatasetItems, function (index, value) {
           var datasetItem = value;
           $('<tr class="tab-cart-items">' +
                //'<td><input name="cart-include" value="' + dataset.Id + '_' + datasetItem.Path + '" type="checkbox" /></td>' +
                '<td> </td>' +
                '<td>' + dataset.Title + '</td>' +
                '<td> </td>' +
                '<td><img src="../../images/filetype_icons/small/' + datasetItem.Format + '.png" alt="' + datasetItem.Format + '" /></td>' +
                // seb-filesize '<td>' + datasetItem.Size + '</td>' +
                '</tr>').appendTo("#tab-cart-table tbody");
       });
       
   });
   setEnablements();

  $("input[name='cart-include']").each(function (index, value) {
      if (!($.inArray($(this).val(), cart.ItemsIgnored) > -1)) {
          $(this).prop('checked', true);
      }
      $(this).click(function (e) {
         // $(this).prop('checked', !$(this).attr('checked'));

          if ($(this).prop('checked')) {
              cart.removeItemToIgnore($(this).attr('value'));
          } else {
              cart.addItemToIgnore($(this).attr('value'));
          }
           setEnablements();
       });
  });
}

function setEnablements() {
    var sizeAmt = cart.getDownloadItemsSize();
    var cartSize = getCartSize();
    $("#cart-spinner").hide();
    $("#cart-total-size").text(sizeAmt);
    if (cartSize == 0) {
        $("#tab-cart-empty").show();
        $("#tab-cart-summary").addClass("sizeTooLarge");
        $("#cart-remove-btn").addClass("disabled");
        $("#cart-download-btn").addClass("disabled");
        //$("#cart-download-msg").html("Please select a dataset for download or remove.");
    } else if (cartSize == cart.ItemsIgnored.length) {
        $("#tab-cart-summary").addClass("sizeTooLarge");
        $("#cart-remove-btn").removeClass("disabled");
        $("#cart-download-btn").addClass("disabled");
        //$("#cart-download-msg").html("Please select a dataset for download or remove.");
        /*
    } else if (sizeAmt > cart.maxDownLoadSize) {
        $("#tab-cart-summary").addClass("sizeTooLarge");
        $("#cart-remove-btn").removeClass("disabled");
        $("#cart-download-btn").addClass("disabled");
        $("#cart-download-msg").html("Your selection exceeds the " + cart.maxDownLoadSize + " limit. <br /> Please deselect items using the check boxes above left.").addClass("sizeTooLarge");
        */
    } else {
        $("#tab-cart-summary").removeClass("sizeTooLarge");
        $("#cart-download-msg").html("").removeClass("sizeTooLarge");
        $("#cart-remove-btn").removeClass("disabled");
        $("#cart-download-btn").removeClass("disabled");
        $("#tab-cart-empty").hide();
    }
}

function getCartSize() {
    var cartSize = 0;
    if (cart.Datasets.length > 0) {
        $.each(cart.Datasets, function (index, dataset) {
            $.each(dataset.DatasetItems, function () {
                cartSize++;
            });
        });
    }
    return cartSize;
}

$().ready(function () {
    getCart();
    $("#show-cart-btn").click(function () {
        $("#tab-cart").toggle();
    });
    $("#cart-close-btn").click(function () {
        $("#tab-cart").hide();
    });
    $("#cart-download-btn").click(function () {
        cart.package();
    });
    /* Remove functionality */
    $("#cart-remove-btn").click(function () {
        cart.removeDataset();
    });
});

