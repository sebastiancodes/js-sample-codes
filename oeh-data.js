﻿/*
 * Provides functionality used by the applications home page, including the 
 * three feeds, carosel and auto-suggest search box.
 */
function DataTab() {
    var self = this;

    self.refreshSearch = function (data, jquerySel, properties) {
        $(jquerySel).remove(".item");
        //$.parseXML()
        $(data).find("metadata").each(function (index, metadata) {
            var item = $(this);
            var itemDiv = $("<div class='item'>").appendTo($(jquerySel));
            //var feedString = "<div class='item'>";
            for (var x = 0; x < properties.length; x++) {
                if (properties[x] == "title") {
                    var titleDiv = $("<div class='" + properties[x] + "'>").appendTo(itemDiv);
                    var uuid = item.children("geonet\\:info").children("uuid").html();
                    var link = $("<a>").attr("href", "#" + uuid).html(item.children("title").html());
                    link.click(function (e) {
                        e.preventDefault();
                        var id = $(e.currentTarget).attr('href').replace("#", "");
                        tabs.selectTab($('a[href$="tab-search"]'));
                        tabs.resultTab.showSingleResult(id);
                    });
                    titleDiv.append(link);
                    itemDiv.append(titleDiv);
                } else if (properties[x] === "updateDate") {
                    var updateDate = $(metadata).children("revisionDate").html();
                    if (updateDate == null) {
                        updateDate = $(metadata).children("creationDate").html();
                    }
                    var feedString = "<div class='updateDate'>" + updateDate + "</div>";
                    itemDiv.append($(feedString));
                } else {
                    var feedString = "<div class='" + properties[x] + "'>";
                    feedString += self.shortenText(item.find(properties[x]).text(), 250);
                    feedString += "</div>";
                    itemDiv.append($(feedString));
                }
            }
        });
    }
    self.getLatestUpdates = function () {
        //var state = new LuceneSearchState();
        //var renderer = new LuceneFeedRenderer(state, "#posts-updated", ["title", "updateDate"]);
        //state.setSearchOrder({ value: "changeDate", order: "" });
        var state = new IdolSearchState();
        var renderer = new IdolFeedRenderer(state, "#posts-updated", ["title", "updateDate"]);
        state.setSearchOrder({ value: "Date", order: "" });
        state.setPage(0);
        state.setHitsPerPage(7);
        state.executeSearch(renderer);
    }

    self.getMostPopular = function () {
        //var state = new LuceneSearchState();
        //var renderer = new LuceneFeedRenderer(state, "#posts-popular", ["title", "updateDate"]);
        //state.setSearchOrder({ value: "popularity", order: "" });
        var state = new IdolSearchState();
        var renderer = new IdolFeedRenderer(state, "#posts-popular", ["title", "updateDate"]);
        state.setSearchOrder({ value: "Geonet_Popularity", order: "numberdecreasing" });
        state.setPage(0);
        state.setHitsPerPage(7);
        state.executeSearch(renderer);
    }

    self.getNewestRecords = function () {
        //var state = new LuceneSearchState();
        //var renderer = new LuceneLatestSearchRenderer(state);
        //state.setSearchOrder({ value: "creationDate", order: "" });
        var state = new IdolSearchState();
        var renderer = new IdolLatestSearchRenderer(state);
        state.setSearchOrder({ value: "Geonet_Release_Date_EpochSeconds", order: "numberDecreasing" });
        state.setPage(0);
        state.setHitsPerPage(3);
        state.executeSearch(renderer);
    }

    self.shortenText = function (text, newCharLength) {
        var charLength = (newCharLength == null) ? 1000 : newCharLength;
        if (text.length <= charLength) {
            return text;
        }
        text = TruncateString(text, newCharLength);
        text = this.replaceNewlines(text);
        return text;
    }

    self.replaceNewlines = function (text) {
        var newlineArray = [0];
        var index = text.indexOf("\n");
        while (index != -1) {
            newlineArray.push(index);
            index = text.indexOf("\n", index + 1);
        }
        newlineArray.push(text.length + 1);
        var newText = "";
        for (var i = 1; i < newlineArray.length; i++) {
            newText = newText + "<p>" + text.substring(newlineArray[i - 1], newlineArray[i]) + "</p>";
        }
        return newText;
    }
}

$(document).ready(function () {
    tabs.dataTab.getLatestUpdates();
    tabs.dataTab.getMostPopular();
    tabs.dataTab.getNewestRecords();

    /* 15904 10/04/2015 activate enter event on sreach field */
    $("#data-tab-search .search-field").keypress(function (e) {
        //$("#dialog").dialog("open");
        if (e.which == 13) {
            var searchVal = $("#data-tab-search .search-field").prop("value");
            tabs.selectTab($('a[href$="tab-search"]'));
            $('.search-field').val(searchVal); // 10/04/2015 Clear the search fields
            tabs.searchTab.anySearch(searchVal);
        }
    });
    
    $("#data-tab-search .search-button").click(function (e) {
        //$("#dialog").dialog("open");
        var searchVal = $("#data-tab-search .search-field").prop("value"); 
        tabs.selectTab($('a[href$="tab-search"]'));
        $('.search-field').val(searchVal); // 9/04/2015 Clear the search fields
        tabs.searchTab.anySearch(searchVal);

    });

    $('#slider1').tinycarousel({ interval: true, animationTime: 600 });

    $("#slider1 ul.overview li").each(function (index, value) {
        var topic = $(this);
        var image = topic.find("img");
        image.hover(function () {
            $(this).attr("src", "../../Images/topic_icons/large/" + topic.attr("id") + "_on.png") ;
        },
        function () {
            $(this).attr("src", "../../Images/topic_icons/large/" + topic.attr("id") + "_off.png");
        });
23
        topic.click(function () {
            var variables = {
                hitsPerPage: 5,
                request: "search",
                fast: "index",
                from: 1,
                to: 5
            };

            //$("#dialog").dialog("open");

            tabs.selectTab($('a[href$="tab-search"]'));
            tabs.searchTab.clearSearch();
            tabs.searchTab.getSearchState().addSearchTerm("Geonet_Topic_Filter", image.attr("alt"));
            tabs.searchTab.search();
            });
    });

    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    $( "#data-search-field")
// don't navigate away from the field on tab when selecting an item
.bind( "keydown", function( event ) {
    if ( event.keyCode === $.ui.keyCode.TAB &&
    $( this ).data( "ui-autocomplete" ).menu.active ) {
        event.preventDefault();
    }
})
.autocomplete({
    source: function (request, response) {
        /* Testing */
        var path = "../../Proxy.ashx?http://goulbwb40/geonetworkmaint/srv/eng/main.search.suggest?field=any";
        /* Development
        var path = "../../Proxy.ashx?http://goulbwb30/geonetworkmaint/srv/eng/main.search.suggest?field=any";
        */
       /* Local
        var path = "../../Proxy.ashx?http://localhost:8080/geonetworkmaint/srv/eng/main.search.suggest?field=any";
       */
        $.getJSON(path, {
            sortBy:"STARTSWITHFIRST",
            q: extractLast( request.term )
        }, function (data, textStatus, jqXHR) {
            var items = [];
            $.each(data[1], function (key, val) {
                items[key] =val;
            });
            response(items);
        });
    },
    search: function() {
        // custom minLength
        var term = extractLast( this.value );
        if ( term.length < 2 ) {
            return false;
        }
    },
    focus: function() {
        // prevent value inserted on focus
        return false;
    },
    select: function( event, ui ) {
        var terms = split( this.value );
        // remove the current input
        terms.pop();
        // add the selected item
        terms.push( ui.item.value );
        // add placeholder to get the comma-and-space at the end
        terms.push( "" );
        this.value = terms.join( ", " );
        return false;
    }
});
});
