﻿var tabs;

/*
 * Handles the basic functions of the main navigation bar.  Because of the way
 * the template we were given was organised, we couldn't put the tab divs in the 
 * same parent div as the nav links, so we had to build our own listeners.
 */
function Tabs() {
    var self = this;

    /*
     * Registers the click handlers.
     */
    self.initialise = function() {
        $('a.tab').click(function (e) {
            e.preventDefault();
            tabs.selectTab(e.currentTarget);
        });
        this.resultTab = new ResultTab();
        this.searchTab = new SearchTab();
        this.dataTab = new DataTab();
    }

    /*
     * Takes the anchor element of the tab as an arguement, not the list item element.
     */
    self.selectTab = function (tab) {
        var id = $(tab).attr('href');
        $('.active').removeClass('active');
        $('.tab-show').removeClass('tab-show');
        $(tab).parent().addClass('active');
        $(id).addClass('tab-show');
    }

    /*
     * Returns the id (as extracted from the anchor's href) of the selected tab.  In the
     * unlikely event that multiple tabs are selected, it returns the first.
     */
    self.selectedTab = function () {
        var id = $('.active > a').attr('href');
        return id;
    }


    self.showResultList = function () {
        // Something, something, search, something

    }
}

var dataTabs;

/*
 * This object handles the activation of the sub-tabs on the results page.
 */
function DataTabs(tabs) {
    var self = this;
    self.tabs = tabs;

    self.initialise = function () {
        $('a.data-tab').click(function (e) {
            e.preventDefault();
            dataTabs.selectTab(e.currentTarget);
        });
    }

    self.selectTab = function (tab) {
        var id = $(tab).attr('href');
        $('.dt-active').removeClass('dt-active');
        $(tab).parent().addClass('dt-active');
        $('.dt-show').removeClass('dt-show');
        $(id).addClass('dt-show');
    }
}


var dataPostTabs;

function DataPostTabs(tabs) {
    var self = this;
    self.tabs = tabs;

    self.initialise = function () {
        $('a.data-posts-tab').click(function (e) {
            e.preventDefault();
            dataPostTabs.selectTab(e.currentTarget);
        });
    }

    self.selectTab = function (tab) {
        var tabLink = $(tab);
 
        var id = tabLink.attr('href');
        $('.dpt-active').removeClass('dpt-active');
        $(tab).parent().addClass('dpt-active');
        $('.dpt-show').removeClass('dpt-show');
        $(id).addClass('dpt-show');
    }
}

function showDialog(message) {
    $("#dialog-confirm").html(message);
    // Define the Dialog and its properties.

    $("#dialog-confirm").dialog({
        resizable: false,
        modal: true,
        title: "Message",
        height: 200,
        width: 250
    });
}

$().ready(function () {
    tabs = new Tabs();
    tabs.initialise();
    dataPostTabs = new DataPostTabs(tabs);
    dataPostTabs.initialise();
    tabs.showResultList();
    dataTabs = new DataTabs(tabs);
    dataTabs.initialise();
});