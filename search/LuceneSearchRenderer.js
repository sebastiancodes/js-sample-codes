﻿/*
 * Implements the Lucene specific parsing of search result data.
 */
function LuceneSearchRenderer(state) {
    var self = this;

    SearchRenderer.call(self, state);

    /*
     * Search engine specific parsing of the summary record.
     */
    self.buildSummaryStructure = function (data) {
        var summary = $(data).find("summary");
        var result = [];
        result.push(self.buildGroupStructure("Topics", "OEHTopic",
            $(summary).children("Topics").children("Topic"), self.topicMapper.getPrettyName, 13));
        result.push(self.buildGroupStructure("Resource formats", "mimetype",
            $(summary).children("mimetypes").children("mimetype"),
            self.imageHandler.resourceFormatMapper));
        result.push(self.buildGroupStructure("Keywords", "keyword",
            $(summary).children("keywords").children("keyword")));
        return result;
    }

    /*
     * Search engine specific parsing for a single group of summary stats.
     */
    self.buildGroupStructure = function (name, searchGroup, childArray, nameMapper, maxNoHide) {
        var result = {};
        nameMapper = typeof nameMapper !== 'undefined' ? nameMapper : function (name) { return name };
        result.name = name;
        result.searchTerm = searchGroup;
        if (maxNoHide === undefined) {
            result.maxNoHide = 7;
        } else {
            result.maxNoHide = maxNoHide;
        }
        var terms = [];
        childArray.each(function (index, item) {
            var term = {};
            term.count = $(item).attr("count");
            term.plainName = $(item).attr("name");
            term.fancyName = nameMapper(term.plainName);
            terms.push(term);
        });
        result.terms = terms;
        return result;
    }

    /*
     * Search engine specific parsing of the metadata xml document.
     */
    self.buildMetadataStructure = function (data) {
        var allResults = [];
        //alert(data);
        //alert($(data));
        //alert($(data).children("metadata").length);
        $(data).find("metadata").each(function (index, metadata) {
            //alert($(metadata));
            var result = {};
            var topics = [];
            $(metadata).children('topic').each(function (index, topic) {
                var name = $(topic).text();
                topics[index] = name;
            });
            result.topics = topics;
            result.uuid = $(metadata).children("geonet\\:info").children("uuid").text();
            result.title = $(metadata).children("title").text();
            var updateDate = $(metadata).children("revisionDate").text();
            if (updateDate == null) {
                updateDate = $(metadata).children("creationDate").text();
            }
            if (updateDate != null) {
                result.updateDate = updateDate;
            }
            if($(metadata).children("creationDate").text() != null) {
                result.creationDate = $(metadata).children("creationDate").text();
            }
            result.abstract = $(metadata).children("abstract").text();

            result.linkages = [];
            $(metadata).children("linkage").each(function (index, lnk) {
                result.linkages.push(lnk);
            });
            allResults.push(result);
        });
        return allResults;
    }
}

/*
 * Utility renderer that will handle the latest feed on the application
 * home page.
 */
function LuceneLatestSearchRenderer(state) {
    var self = this;

    LuceneSearchRenderer.call(self, state);

    self.renderSummarySections = function (data) { }

    self.updateCount = function (data) { }

    self.setupPage = function () { }

    self.renderMetadataSections = function (data) {
        var container = $("#posts-latest");
        var metadata = self.buildMetadataStructure(data);
        var properties = ["title", "abstract"];

        container.remove(".item");
        //$.parseXML()
        $(metadata).each(function (index, metadata) {
            var itemDiv = $("<div class='item'>").appendTo(container);
            $("<div class='image'><img src='../../images/topic_icons/default-new-releases.png' /></div>").appendTo(itemDiv);
            var bodyDiv = $("<div class='body'>").appendTo(itemDiv);
            for (var x = 0; x < properties.length; x++) {
                if (properties[x] == "title") {
                    var titleDiv = $("<div class='" + properties[x] + "'>").appendTo(bodyDiv);
                    var uuid = metadata.uuid;
                    var link = $("<a>").attr("href", "#" + uuid).html(metadata.title);
                    link.click(function (e) {
                        e.preventDefault();
                        var id = $(e.currentTarget).attr('href').replace("#", "");
                        tabs.selectTab($('a[href$="tab-search"]'));
                        tabs.resultTab.showSingleResult(id);

                    });
                    titleDiv.append(link);
                    bodyDiv.append(titleDiv);
                } else {
                    var feedString = "<div class='" + properties[x] + "'>";
                    feedString += self.shortenText(metadata[properties[x]], 200);
                    feedString += "</div>";
                    bodyDiv.append($(feedString));
                }
            }
            //TODO: LISAsoft wd - we must fix image icons currently just set to default
        });
    }
}

/*
 * Utility renderer that will handle the right-hand feeds on the application
 * home page.
 */
function LuceneFeedRenderer(state, jquerySelIn, propertiesIn) {
    var self = this;
    var jquerySel = jquerySelIn;
    var properties = propertiesIn;

    LuceneSearchRenderer.call(self, state);

    self.renderSummarySections = function (data) { }

    self.updateCount = function (data) { }

    self.setupPage = function (data) { }

    self.renderMetadataSections = function (data) {
        var metadata = self.buildMetadataStructure(data);
        $(jquerySel).remove(".item");
        //$.parseXML()
        $(metadata).each(function (index, metadata) {
            var itemDiv = $("<div class='item'>").appendTo($(jquerySel));
            //var feedString = "<div class='item'>";
            for (var x = 0; x < properties.length; x++) {
                if (properties[x] == "title") {
                    var titleDiv = $("<div class='" + properties[x] + "'>").appendTo(itemDiv);
                    var uuid = metadata.uuid;
                    var link = $("<a>").attr("href", "#" + uuid).html(metadata.title);
                    link.click(function (e) {
                        e.preventDefault();
                        var id = $(e.currentTarget).attr('href').replace("#", "");
                        tabs.selectTab($('a[href$="tab-search"]'));
                        tabs.resultTab.showSingleResult(id);
                    });
                    titleDiv.append(link);
                    itemDiv.append(titleDiv);
                } else if (properties[x] === "updateDate") {
                    var updateDate = metadata.revisionDate;
                    if (!updateDate) {
                        updateDate = metadata.creationDate;
                    }
                    if (!updateDate) {
                        updateDate = '';
                    }
                    var feedString = "<div class='updateDate'>" + updateDate + "</div>";
                    itemDiv.append($(feedString));
                } else {
                    var feedString = "<div class='" + properties[x] + "'>";
                    feedString += self.shortenText(metadata[properties[x]], 250);
                    feedString += "</div>";
                    itemDiv.append($(feedString));
                }
            }
        });
    }
}