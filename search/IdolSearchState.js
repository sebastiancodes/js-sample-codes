﻿
/*
 * Manages the state of the search parameters and produces search query strings appropriate for the Idol search.
 */
function IdolSearchState() {
    var self = this;

    SearchState.call(self);

    /*
     * Idol requires a separate call to retrieve summary details for 
     * the left filter bar.
     */
    self.getSummaryData = function () {
        var string = "http://141.243.3.19:10000/action=GetQueryTagValues";
        /*
        http://141.243.3.19:10000/action=GetQueryTagValues&
        FieldName=Geonet_Keyword&
        Text=*&
        DocumentCount=true&
        MaxValues=25&
        Start=1&
        SecurityInfo=MjQ0fFyfRcY%2F%2BBM1aj5naYe%2FwQA1UCubCb2p3Yp6Wm2R62cL9AvjQaY2ire5bH%2B0bgcnB28%2BOaQwsHB%2FtdvXJZ4EEyT5Ml4g3AAYrVJmacFQ9aSQybSzFJlYddIM4VoZbRucNebRufGp0ooH5IRbxB7wZ%2F4zlxK9JWxZ3HIAsDcSUVImTltcyWVlVNxvCJ6prd%2FnQqBZySwT7VEIlkv3rtCXfn96odGq4sf2V%2BkNiEQz%2Ffy3zpb%2BGTzExT8Wj25yRS6KtUItBWipjlhBqpcM5MhSDPNeR98B1NutIhz9tkhr4blSMUM0icl%2FmLewgqtjL%2FFXBQldfjE%3D
        */

        /* Adjust with any parametric field names we need. */
        string = string + "&FieldName=Geonet_Keyword,Geonet_Distribution_Format,Geonet_Topic";
        string = string + "&DocumentCount=true";
        /* Adjust to set the number of values to return. */
        string = string + "&MaxValues=25";
        string = string + "&Start=1";
        string = string + "&Sort=DocumentCount";

        string = string + "&" + self.generateSearchFilter();

        return string;
    }
    /*
     * Because the handling of array values by jQuery is different than what GeoNetwork expects,
     * we actually have to make this a string ourselves.
     */
    self.getSearchData = function () {
        /* TBD: Standard query parameters */
        var string = "http://141.243.3.19:10000/action=Query";

        /* Paging parameters */
        var hits = parseInt(self.searchData['hitsperpage']);
        // We're using 0-based internally, but the request must be 1-based
        var page = parseInt(self.searchData['page']);
        var from = (page * hits) + 1;
        var to = (from + hits) - 1;

        string = string + "&MaxResults=" + to;
        string = string + "&Start=" + from;

        /* Sorting parameters */
        /* TBD: 
           &Sort=Relevance
           &Sort=Date
           &Sort=DreTitle:alphabetical
         */
        if (self.sortBy != '') {
            string = string + "&Sort=" + self.sortBy;
        }
        if (self.sortOrder != '') {
            string = string + ":" + self.sortOrder;
        }
        string = string + "&" + self.generateSearchFilter();
        return string;
    }

    self.getSecurityInfo = function () {
        /*
        var requestPath = "../../Proxy.ashx?http://141.243.3.19:10000/action=UserRead&username=Someones+Name&SecurityInfo=true";
        var secInfo = '';
        $.ajax({
            async: false,
            url: requestPath,
            success: function (data, textStatus, jqXHR) {
                var val = $(data).children("autnresponse").children("responsedata").children("autn\\:securityinfo").text();
                secInfo = val;
            }
        });       
        return encodeURIComponent(secInfo);
        */
        return '';
    }

    self.generateSearchFilter = function () {
        /*
        http://141.243.3.19:10000/
        action=Query&
        Text=air+quality&
        DatabaseMatch=Geonetwork&
        Print=All&
        MaxResults=6&
        TotalResults=true&
        SecurityInfo=...&
        FieldText=GREATER{-35}:Geonet_Extent_South+AND+LESS{-30}:Geonet_Extent_North+AND+GREATER{139}:Geonet_Extent_West+AND+LESS{145}:Geonet_Extent_East
        */
        /* 11/05/2015 Sebastian - Case ID: 28000 - Separate IDOL Search Engines */
        /* Development */
        var string = "DatabaseMatch=Geonetwork-Test&TotalResults=true";
        /* Test 
        var string = "DatabaseMatch=Geonetwork-Test&TotalResults=true";
        */
        /* We need to list all of the fields we actually depend on here. */
        string = string + "&PrintFields=Geonet_Popularity,Geonet_Updated_Date,Geonet_Created_Date,DREREFERENCE,CREATED_DATE,DRETITLE,GEONET_ABSTRACT,GEONET_TOPIC,GEONET_TRANSFER_PROTOCOL_*,GEONET_TRANSFER_URL_*,GEONET_TRANSFER_NAME_*,GEONET_TRANSFER_DESCRIPTION_*&Predict=false";
        /* Specific handling for the 'any' term */
        var any = self.searchTerms.any;
        if (any && any != '') {
            if ($.isArray(any)) {
                string = string + "&Text=" + any.join("+");
            } else {
                string = string + "&Text=" + any;
            }
        } else {
            string = string + "&Text=*";
        }

        var fieldText = [];
        /* only retrieves approved data */
        fieldText.push("MATCH{2}:Geonet_Status");
        /* Specific handling for date terms */
        var fromDate = self.searchTerms.extFrom;
        var toDate = self.searchTerms.extTo;
        if (fromDate && toDate) {
            fromDate = convertDateString(fromDate);
            toDate = convertDateString(toDate);
            fieldText.push("RANGE{" + fromDate + "," + toDate + "}:GEONET_UPDATED_DATE");
        }

        /* Remaining search term parameters */
        jQuery.each(self.searchTerms, function (key, term) {
            if( term != null && term != '') {
                if (key != 'any' && key != 'extTo' && key != 'extFrom') {
                    if ($.isArray(term)) {
                        $(term).each(function (index, value) {
                            fieldText.push("MATCH{" + value + "}:" + key);
                        });
                    } else {
                        fieldText.push("MATCH{" + term + "}:" + key);
                    }
                }
            }
        });

        /* Extent parameters */
        var extents = self.extents;
        if (!isNaN(extents.xmin) && !isNaN(extents.ymin) && !isNaN(extents.xmax) && !isNaN(extents.ymax)) {
            fieldText.push("GREATER{" + extents.xmin + "}:Geonet_Extent_West");
            fieldText.push("GREATER{" + extents.ymin + "}:Geonet_Extent_South");
            fieldText.push("LESS{" + extents.xmax + "}:Geonet_Extent_East");
            fieldText.push("LESS{" + extents.ymax + "}:Geonet_Extent_North");
        }
        if (fieldText.length > 0) {
            string = string + "&FieldText=" + fieldText.join("+AND+");
        }
        var securityInfo = self.getSecurityInfo();
        if (securityInfo != '') {
            string = string + "&SecurityInfo=" + securityInfo;
        }
        /*  15903 - Sebastian 21/04/2015 OpenOEH Home Page Boolean Searches*/
        string = string + "&MatchAllTerms=true";
        return string;
    }

    function convertDateString(dateIn) {
        var bits = dateIn.split('-');
        if (bits.length != 3) {
            return;
        }
        var dateOut = bits[2] + "/" + bits[1] + "/" + bits[0];
        return dateOut;
    }

    self.executeSearch = function (searchRenderer) {
        var searchPath = "../../Proxy.ashx?" + self.getSearchData();
        searchRenderer.setupPage();
        $.post(searchPath, function (data, textStatus, jqXHR) {
            searchRenderer.renderMetadataSections(data);
            var count = $(data).children("autnresponse").children("responsedata").children("autn\\:totalhits").text();
            searchRenderer.updateCount(count);
        }).fail(function (data, textStatus, jqXHR) {
            console.log("Idol search failed. " + textStatus + data + jqXHR);
        });
        var summaryPath = "../../Proxy.ashx?" + self.getSummaryData();
        $.post(summaryPath, function (data, textStatus, jqXHR) {
            searchRenderer.renderSummarySections(data);
        }).fail(function (data, textStatus, jqXHR) {
            console.log("Idol summary search failed. " + textStatus + data + jqXHR);
        });
    }
}

