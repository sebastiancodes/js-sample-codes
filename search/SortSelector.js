﻿
/*
 * Generates and lays out the select element for sort order.
 */
function SortSelector(searchFunc, updateStateFunc, sortByData) {
    var self = this;
    var search = searchFunc;
    var updateState = updateStateFunc;

    self.sortBy = [
        { display: 'Relevance', value: 'relevance', order: '', enabled: true },
        { display: 'Last Updated', value: 'Date', order: '', enabled: false },
        { display: 'Title', value: 'title', order: 'reversed', enabled: false }
    ];
    updateState(self.sortBy[0]);

    self.buildSortSelect = function () {
        var result = $("<div id='sortDiv'>");

        var dd = $('<span>').appendTo(result).addClass('dropdown').addClass('btn');
        var btn = $('<a>').appendTo(dd).attr('data-toggle', 'dropdown').attr('href', '#');

        var list = $('<ul>').appendTo(dd).addClass('dropdown-menu').attr('aria-labelledby', 'dLabel');
        $(self.sortBy).each(function (index, order) {
            var li = $('<li>').appendTo(list).attr('role', 'presentation');
            var display = order.display
            var a = $('<a>').appendTo(li).attr('role', 'menuitem').attr('href', '#').html(display);
            if (order.enabled) {
                btn.html(display);
            }
            a.click(function (event) {
                event.preventDefault();
                //updateState(order);
                //search();

                $("a [data-toggle=dropdown]").html(order);
                $(self.sortBy).each(function (newIndex, newOrder) {
                    if (newOrder.display == order.display) {
                        newOrder.enabled = true;
                    } else {
                        newOrder.enabled = false;
                    }
                });
                updateState(order);
                search();
            });
        });
        $('<span>').appendTo(btn).addClass('caret');

        return result;
    }
}
