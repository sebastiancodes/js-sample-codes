﻿
/*
 * Generates the ranges required for date filtering and lays out the selection box.
 */
function DateSelector(state, renderer) {
    var self = this;
    var searchState = state;

    self.buildDateSelector = function () {
        var li1 = $("<li>").html("Last updated");
        var ul2 = $("<ul>").appendTo(li1);
        var li2 = $("<li>").appendTo(ul2);
        
        var dd = $('<span>').appendTo(li2).addClass('dropdown').addClass('btn');
        var btn = $('<a>').appendTo(dd).attr('data-toggle', 'dropdown').attr('href', '#').html('Any date');

        var list = $('<ul>').appendTo(dd).addClass('dropdown-menu').attr('aria-labelledby', 'dLabel');
        var today = new Date();
        var dateBlocks = self.getDateBlocks(today);
        $(dateBlocks).each(function (index, item) {
            var li = $('<li>').appendTo(list).attr('role', 'presentation');
            var a = $('<a>').appendTo(li).attr('role', 'menuitem').attr('href', '#').html(item.name);
            if (item.name == searchState.dateValue) {
                btn.html(item.name);
            }
            a.click(function (event) {
                event.preventDefault();
                searchState.removeSearchTerms('extFrom');
                searchState.removeSearchTerms('extTo');
                searchState.dateValue = item.name;
                if (item.start) {
                    var dt = new Date(item.start);
                    var str = dt.toISOString();
                    searchState.addSearchTerm('extFrom', str.substring(0, str.indexOf('T')));
                }
                if (item.end) {
                    var dt = new Date(item.end);
                    var str = dt.toISOString();
                    searchState.addSearchTerm('extTo', str.substring(0, str.indexOf('T')));
                }
                searchState.setPage(0);
                searchState.executeSearch(renderer);
            });
        });
        $('<span>').appendTo(btn).addClass('caret');
    
        /*sel.customSelect({ customClass: 'search-select' });*/
        return li1;
    }

    self.getDateBlocks = function (today) {
        var dateRanges = [];
        dateRanges.push({ name: "Any date" });

        var month = today.getMonth();
        var year = today.getYear();
        var m = new Date(today);
        m.setDate(1);
        m.setMonth(today.getMonth() - 1);
        dateRanges.push({ name: "In the last month", start: m, end: today });

        var q = new Date(today);
        q.setDate(1);
        q.setMonth(today.getMonth() - 3);
        dateRanges.push({ name: "In the last three months", start: q, end: today });

        var h = new Date(today);
        h.setDate(1);
        h.setMonth(today.getMonth() - 6);
        dateRanges.push({ name: "In the last six months", start: h, end: today });

        var y = new Date(today);
        y.setDate(1);
        y.setMonth(0);
        y.setFullYear(today.getFullYear() - 1);
        dateRanges.push({ name: "In the last year", start: y, end: today });

        var y2 = new Date(today);
        y2.setDate(1);
        y2.setMonth(0);
        y2.setFullYear(today.getFullYear() - 2);
        dateRanges.push({ name: "In the last two years", start: y2, end: today });

        var y5 = new Date(today);
        y5.setDate(1);
        y5.setMonth(0);
        y5.setFullYear(today.getFullYear() - 5);
        dateRanges.push({ name: "In the last five years", start: y5, end: today });

        var d = new Date(today);
        d.setDate(1);
        d.setMonth(0);
        d.setFullYear(today.getFullYear() - 10);
        dateRanges.push({ name: "In the last ten years", start: d, end: today });

        return dateRanges;
    }

    self.oldgetDateBlocks = function (today) {
        var thisMonth = today.getMonth();
        var i = thisMonth;
        var dateRanges = [];

        /* All months in the current incomplete quarter */
        for (; (i >= 0) && (i % 3) != 2; i--) {
            var start = new Date(today);
            start.setMonth(i);
            start.setDate(1);
            var end = new Date(today);
            end.setMonth(i + 1)
            end.setDate(0);

            var name = PrintableMonth(i);
            dateRanges.push({ name: name, start: start, end: end, type: "Month" });
        }

        /* All quarters in the current incomplete year */
        i = Math.floor(i / 3);
        for (; i >= 0; i--) {
            var start = new Date(today);
            start.setMonth(i * 3);
            start.setDate(1);
            var end = new Date(today);
            end.setMonth((i + 1) * 3);
            end.setDate(0);

            var name = PrintableQuarter(i, start.getFullYear());
            dateRanges.push({ name: name, start: start, end: end, type: "Quarter" });
        }

        /* All years in the current incomplete decade */
        var year = today.getFullYear() - 1;
        for (; (year > 0) && (year % 10 != 9) ; year--) {
            var start = new Date(today);
            start.setDate(1);
            start.setMonth(0);
            start.setYear(year);
            var end = new Date(today);
            end.setYear(year + 1);
            end.setMonth(0);
            end.setDate(0);

            var name = year.toString();
            dateRanges.push({ name: name, start: start, end: end, type: "Year" });
        }

        /* All decades in the current incomplete century */
        var decade = Math.floor(today.getFullYear() / 10);
        for (; (decade > 0) && (decade % 10) != 9; decade--) {
            var start = new Date(today);
            start.setDate(1);
            start.setMonth(0);
            start.setYear(decade * 10);
            var end = new Date(today);
            end.setYear((decade + 1) * 10);
            end.setMonth(0);
            end.setDate(0);

            var name = (decade * 10).toString() + "s";
            dateRanges.push({ name: name, start: start, end: end, type: "Decade" });
        }

        /* Everything else */
        var start = new Date(0);
        var end = new Date(today);
        var year = today.getFullYear();
        year = Math.floor(year / 100) * 100;
        end.setYear(year);
        end.setMonth(0);
        end.setDate(0);
        var name = "Before " + (year);
        dateRanges.push({ name: name, start: start, end: end, type: "Decade" });

        return dateRanges;
    }
}