﻿/*
 * This object provides lookup methods use to map the ugly/machine readable name of a topic to the 
 * pretty/human readable name.
 */
function TopicMapper() {
    var self = this;

    var mapping = [
        { uglyName: 'Aboriginal culture and heritage', prettyName: 'Aboriginal culture and heritage' },
        { uglyName: 'Air and water quality', prettyName: 'Air and water quality' },
        { uglyName: 'Aquatic ecosystems', prettyName: 'Aquatic ecosystems' },
        { uglyName: 'Climate change impacts and adaptation', prettyName: 'Climate change impacts' },
        { uglyName: 'Coastal hazards and flood risks', prettyName: 'Coastal hazards and flood risks' },
        { uglyName: 'Fire and incident management', prettyName: 'Fire and incident management' },
        { uglyName: 'Historic heritage', prettyName: 'Historic heritage' },
        { uglyName: 'Landscapes', prettyName: 'Landscapes' },
        { uglyName: 'Native plants and animals', prettyName: 'Native plants and animals' },
        { uglyName: 'Pests and weeds', prettyName: 'Pests and weeds' },
        { uglyName: 'Protected areas', prettyName: 'Protected areas' },
        { uglyName: 'Sustainable land use and development', prettyName: 'Sustainable land use' },
        { uglyName: 'Vegetation', prettyName: 'Vegetation' }
    ];
    self.getPrettyName = function (uglyName) {
        var prettyName = uglyName;
        $(mapping).each(function (index, topic) {
            if (topic && topic.uglyName == uglyName) {
                prettyName = topic.prettyName;
                return false;
            }
        });
        return prettyName;
    }

    self.getUglyName = function (prettyName) {
        var uglyName = prettyName;
        $(mapping).each(function (index, topic) {
            if (topic && topic.prettyName == prettyName) {
                uglyName = topic.uglyName;
                return false;
            }
        });
        return uglyName;
    }

}