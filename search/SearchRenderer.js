﻿/*
 * Responsible for rendering the details of the search into the page.  
 */
function SearchRenderer(state) {
    var self = this;
    var searchState = state;
    var dateSelector = new DateSelector(searchState, self);
    var pager = new PagerWidget(searchState, self);

    self.sortSelector = new SortSelector(function () { searchState.executeSearch(self) }, searchState.setSearchOrder);
    self.imageHandler = new ImageHandler();
    self.topicMapper = new TopicMapper();

    self.setupPage = function () {
        var sum = $(".search-summary").empty();
        $(".search-results").empty();
        var cntDiv = $("<div id='cntDiv'>").appendTo(sum);
        $("<span>").addClass('count').appendTo(cntDiv).html("0 datasets");
        $("<span>").addClass('found').appendTo(cntDiv).html(" found");
        var sortDiv = self.sortSelector.buildSortSelect().appendTo(sum);

        var anyDiv = $("<div id='anyDiv'>").appendTo(sum);
        $("<span>").appendTo(anyDiv).html("For: ");
        //        var currSearch = (searchState.getSearchTerms("any") == "") ? "No search term" : searchState.getSearchTerms("any");
        var currSearch = "No search term";
        if (searchState.getSearchTerms('any') != "" && searchState.getSearchTerms('any') != undefined) {
            currSearch = searchState.getSearchTerms('any');
        } else if (searchState.getSearchTerms('Geonet_Topic_Filter') != "" && searchState.getSearchTerms('Geonet_Topic_Filter') != undefined) {
            currSearch = searchState.getSearchTerms('Geonet_Topic_Filter');
        }
        $("<span>").appendTo(anyDiv).html(currSearch);
        /*
        self.setupRefinementSearch(data);
        var metadata = self.buildMetadataStructure(data);
        $.each(metadata, function (index, meta) {
            self.addMetadataRecord(meta);
        });
        */
        $(".search-paging").empty();
        //$(".search-paging").append(pager.buildWidget(count));
        $("#tab-search select").customSelect({ customClass: 'search-select' });
    };

    self.updateCount = function (count) {
        $("#cntDiv .count").html(count + " datasets");
        $(".search-paging").append(pager.buildWidget(count));
    }

    self.renderMetadataSections = function (data) {
        var metadata = self.buildMetadataStructure(data);
        $.each(metadata, function (index, meta) {
            self.addMetadataRecord(meta);
        });
    }

    self.renderSummarySections = function (data) {
        self.setupRefinementSearch(data);
    }

    self.renderPageArtifacts = function (data) {
        var count = $(data).children('summary').attr("count")
        $("#cntDiv .count").html(count + " datasets");
        $(".search-paging").append(pager.buildWidget(count));
    }

    /*
     * Builds the refinement search bar (left) using the xml results from
     * the search service.  Delegates parsing of the xml to
     * SearchRenderer.buildSummaryStructure.
     */
    self.setupRefinementSearch = function (xmlSummary) {
        var summary = self.buildSummaryStructure(xmlSummary);
        var barsearch = $("#tab-search .bar-search");
        barsearch.empty();
        var container = $(".search-refine");
        container.empty();

        self.buildFulltextBox(barsearch);
        container.append($("<hr>"));

        container.append($('<h3>').html('Filter results'));

        var outerList = $("<ul>").appendTo(container);
        outerList.append(dateSelector.buildDateSelector());

        $(summary).each(function (index, group) {
            outerList.append(self.buildRefinementList(group));
        });
    }


    /*
     * Build the refinement list structures using the given group object.
     */
    self.buildRefinementList = function (group) {
        var result = $("<li>").html(group.name);
        var list = $("<ul>").appendTo(result);
        var searchRenderer = self;
        $(group.terms).each(function (index, term) {
            var listItem = $("<li title='" + term.fancyName + "'>").appendTo(list).addClass("search-term");
            var termButton = $("<button>").appendTo(listItem).html(term.fancyName + " (" + term.count + ")")
                .attr("searchGroup", group.searchTerm).attr("searchTerm", term.plainName).click(searchRenderer.selectTerm);
            var removeButton = $("<button>").appendTo(listItem).addClass("search-term-remove").click(searchRenderer.deselectTerm);
            if (searchState.hasSearchTerm(group.searchTerm, term.plainName)) {
                termButton.addClass("search-term-selected");
                removeButton.show();
            } else {
                termButton.addClass("search-term-unselected");
                removeButton.hide();
            }

        });
        $("li .search-term").tooltip();
        var maxNoHide = group.maxNoHide;
        if (list.children(".search-term").length > maxNoHide) {
            list.children(".search-term").each(function (index, child) {
                if (index >= (maxNoHide - 2)) {
                    $(child).hide();
                }
            });
            var show = $("<li>").appendTo(list).addClass("search-show");
            var hide = $("<li>").appendTo(list).hide().addClass("search-show");
            $('<a href="#">').html("See more").appendTo(show).click(function (event) {
                event.preventDefault();
                list.children(".search-term").each(function (index, child) {
                    $(child).show();
                });
                show.hide();
                hide.show();
            });
            $('<a href="#">').html("See less").appendTo(hide).click(function (event) {
                event.preventDefault();
                list.children(".search-term").each(function (index, child) {
                    if (index > 5) {
                        $(child).hide();
                    } else {
                        $(child).show();
                    }
                });
                hide.hide();
                show.show();
            });
        }
        return result;
    }

    /*
     * Implementation must produce objects of the following format:
     * [
     *   [
     *     {
     *       "name": "group_name",
     *       "searchTerm": "term_name",
     *       "maxNoHide": 7,
     *       "terms": [
     *         {
     *           "plainName": "blah",
     *           "fancyName": "Human Readable Blah",
     *           "count": 3
     *         }, ...
     *       ]
     *     }, ...
     *   ]
     * ]
     */
    self.buildSummaryStructures = function (summary) {
        throw "SearchRenderer.buildSummaryStructures is abstract and must be extended by a subclass.";
    }

    /*
     * Implementation must produce objects of the following format:
     * [
     *   {
     *     "uuid": "uuidvalue",
     *     "creationDate": "2012-02-22",
     *     "updateDate": "2013-02-34",
     *     "title": "Metadata title",
     *     "abstract": "Long, rambling complete abstract text.",
     *     "topics": ["topicname1", "topicname2", ...],
     *     "linkages": ["linkagename1", "linkagename2", ...]
     *   }
     * ]
     */
    self.buildMetadataStructures = function (metadata) {
        throw "SearchRenderer.buildSummaryStructures is abstract and must be extended by a subclass.";
    }

    self.getSearchByData = function () {
        throw "SearchRenderer.getSearchByData is abstract and must be extended by a subsclass.";
    }

    /*
     * Acceps a single metadata json object, as built by the self.buildMetadataStructures,
     * and renders the display.
     */
    self.addMetadataRecord = function (metadata) {
        var container = $(".search-results");
        var md = $("<div>").appendTo(container);

        $(metadata.topics).each(function (index, topic) {
            var className = "topic-" + topic.toLowerCase().replace(/ /g, "-");
            var tpc = $("<div>").appendTo(md).addClass('search-topic').addClass(className).html(topic);
        });

        var link = $("<a>").attr("href", "#" + metadata.uuid).appendTo(md);
        link.click(function (e) {
            e.preventDefault();
            var id = $(e.currentTarget).attr('href').replace("#", "");
            tabs.resultTab.showSingleResult(id);
        });
        $("<div>").appendTo(link).html(metadata.title).addClass("s-title");

        if (metadata.updateDate != null && metadata.updateDate != '') {
            $("<div>").appendTo(md).html("Updated " + metadata.updateDate).addClass("s-date");
        }

        var abstractSource = metadata.abstract;
        var abstractBody = TruncateString(abstractSource);
        if (abstractBody != abstractSource) {
            abstractBody = abstractBody + " ...";
        }
        $("<div>").appendTo(md).html(abstractBody).addClass("s-abstract");

        var imageDiv = $("<div>").appendTo(md).addClass("s-icons");
        $(metadata.linkages).each(function (index, lnk) {
            var image = self.imageHandler.createSmallFiletypeIcon(lnk);
            if (image !== undefined && image !== null) {
                $(image).appendTo(imageDiv);
            }
        });
    }

    /*
     * This will be triggered by the term button, first in the line.  It will need to show the remove button, change its own class,
     * add the term to the search, remove its click handler and refresh the search.
     */
    self.selectTerm = function (e) {
        var termButton = $(e.currentTarget);
        termButton.removeClass('search-term-unselected').addClass('search-term-selected');
        var removeButton = termButton.parent().children("button:last-child");
        removeButton.show();
        var group = termButton.attr("searchGroup");
        var term = termButton.attr("searchTerm");
        searchState.addSearchTerm(group, term);
        searchState.setPage(0);
        state.executeSearch(self);
    }

    /*
     * This will be triggered by the second button, the X.  It will need to hide itself, change the class of the tag button,
     * remove the search term and apply the click handler.
     */
    self.deselectTerm = function (e) {
        var removeButton = $(e.currentTarget);
        removeButton.hide();
        var termButton = removeButton.parent().children("button:first-child");
        termButton.removeClass('search-term-selected').addClass('search-term-unselected');
        var group = termButton.attr("searchGroup");
        var term = termButton.attr("searchTerm");
        searchState.removeSearchTerm(group, term);
        state.executeSearch(self);
    }

    self.buildFulltextBox = function (container) {
        var h3 = $('<h3>').appendTo(container).html('Search ');
        //<button type="button" class="btn btn-primary btn-sm">Small button</button>
        $('<button>').appendTo(h3).attr('type', 'button').addClass('btn').addClass('btn-default').addClass('pull-right').addClass('btn-sm').html('Clear').click(function(event) {
            searchState.clear();
            $('.search-field').val(''); // 9/04/2015 Clear the search fields 
            searchState.executeSearch(self);
        });
        //<span class="label label-default label-sm">Default</span>
        var searchList = $('<li>').appendTo($('<ul>').appendTo(container));
        var searchField = $('<input>').appendTo(searchList).addClass('search-field');
        var searchButton = $('<button>').appendTo(searchList).html('Search').addClass('search-button');

        searchField.attr('value', searchState.getSearchTerms('any'));
        /* 15904  10/04/2015 activate enter event on sreach field */
        searchField.keypress(function (e) {
            if (e.which == 13) {
                var terms = searchField.val();
                searchState.removeSearchTerms('any');
                if (terms != '') {
                    searchState.addSearchTerm('any', terms);
                }
                $('.search-field').val(terms); // 10/04/2015 Clear the search fields 
                searchState.executeSearch(self);
            }
        });
        
        searchButton.click(function (e) {
            var terms = searchField.val();
            searchState.removeSearchTerms('any');
            if (terms != '') {
                searchState.addSearchTerm('any', terms);
            }
            $('.search-field').val(terms); // 9/04/2015 Clear the search fields 
            searchState.executeSearch(self);
        });
    }

    self.shortenText = function (text, newCharLength) {
        var charLength = (newCharLength == null) ? 1000 : newCharLength;
        if (text.length <= charLength) {
            return text;
        }
        text = TruncateString(text, newCharLength);
        text = this.replaceNewlines(text);
        return text;
    }

    self.replaceNewlines = function (text) {
        var newlineArray = [0];
        var index = text.indexOf("\n");
        while (index != -1) {
            newlineArray.push(index);
            index = text.indexOf("\n", index + 1);
        }
        newlineArray.push(text.length + 1);
        var newText = "";
        for (var i = 1; i < newlineArray.length; i++) {
            newText = newText + "<p>" + text.substring(newlineArray[i - 1], newlineArray[i]) + "</p>";
        }
        return newText;
    }
}