﻿function StrategyMapper(strategy, searchType1) {
    var self = this;
    this.strategy = strategy;
    this.searchType = (searchType1 !=undefined)?searchType1:"";
    var state = "";
    var renderer = "";
    var order = "";

    self.getSearchState = function () {
        if (strategy == "LUCENE") {
            self.state = new LuceneSearchState();
            if (self.searchType == undefined)
                self.order = "";
            else if (self.searchType.toUpperCase() == "LATEST")
                self.order = { value: "changeDate", order: "" };
            else if (self.searchType.toUpperCase() == "POPULAR")
                self.order = { value: "popularity", order: "" };
            else if (self.searchType.toUpperCase() == "NEWEST")
                self.order = { value: "creationDate", order: "" };
            else
                self.order = "";
               
        } else if (strategy == "IDOL") {
            self.state = new IdolSearchState();

            if (self.searchType == undefined)
                self.order = "";
            else if (self.searchType.toUpperCase() == "LATEST")
                self.order = { value: "Geonet_Updated_Date", order: "decreasing" };
            else if (self.searchType.toUpperCase() == "POPULAR")
                self.order = { value: "Geonet_Popularity", order: "decreasing" };
            else if (self.searchType.toUpperCase() == "NEWEST")
                self.order = { value: "Geonet_Created_Date", order: "decreasing" };
            else
                self.order = "";
        }
        self.state.setSearchOrder(self.order);
        return self.state;
    }

    self.getRenderer = function (state, id, fields) {
        if (self.strategy == "LUCENE") {

            if (self.searchType == undefined)
                return new LuceneSearchRenderer(state);
            else if (self.searchType.toUpperCase() == "NEWEST")
                return new LuceneLatestSearchRenderer(state);
            else if (self.searchType.toUpperCase() == "LATEST" || self.searchType.toUpperCase() == "POPULAR")
                return new LuceneFeedRenderer(state, id, fields);
            else 
                return new LuceneSearchRenderer(state);
            
        } else if (self.strategy == "IDOL") {

            if (self.searchType == undefined)
                return new IdolSearchRenderer(state);
            else if (self.searchType.toUpperCase() == "NEWEST")
                return new IdolLatestSearchRenderer(state);
            else if (self.searchType.toUpperCase() == "LATEST" || searchType.toUpperCase() == "POPULAR")
                return new IdolFeedRenderer(state, id, fields);
            else
                return new IdolSearchRenderer(state);
        }
    }
}