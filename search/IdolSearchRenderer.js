﻿/*
 * Implements the Idol specific parsing of search result data.
 */
function IdolSearchRenderer(state) {
    var self = this;

    SearchRenderer.call(self, state);

    self.sortSelector.sortBy = [
            { display: 'Relevance', value: 'Relevance', order: '', enabled: true },
            { display: 'Last Updated', value: 'Date', order: '', enabled: false },
            { display: 'Title', value: 'DreTitle', order: 'alphabetical', enabled: false }
        ];

    self.buildSummaryStructure = function (summary) {
        var results = [];
        $(summary).children("autnresponse").children("responsedata").children("autn\\:field").each(function (index, field) {
            var declaration = $(field).children("autn\\:name").text();
            var searchTerm = declaration.substring(declaration.lastIndexOf("/") + 1, declaration.length)
            var name = toTitleCase(searchTerm);
            var sortOrder = 'a';
            if (name == 'Geonet_keyword') {
                name = 'Keywords';
                sortOrder = 3;
            } else if (name == 'Geonet_distribution_format') {
                name = 'Distribution Format';
                sortOrder = 2;
            } else if (name == 'Geonet_topic') {
                name = 'Topics';
                searchTerm = 'GEONET_TOPIC_FILTER';
                sortOrder = 1;
            }
            var values = [];
            $(field).children("autn\\:value").each(function (i, value) {
                var count = $(value).attr("count");
                var word = $(value).text();
                var value = { plainName: word, fancyName: toTitleCase(word), count: count };
                values.push(value);
            });
            var result = { name: name, searchTerm: searchTerm, maxNoHide: 7, terms: values, sortOrder: sortOrder };
            results.push(result);
        });

        results.sort(function (a, b) {
            var sa = a.sortOrder != undefined ? a.sortOrder.toString() : 'z';
            var sb = b.sortOrder != undefined ? b.sortOrder.toString() : 'z';
            return a.sortOrder > b.sortOrder
        });

        return results;
    }

    self.buildMetadataStructure = function (metadata) {
        /*
        autnresponse/responsedata/autn:hit
        autn:content/Document
        */
        var results = [];
        var mimeTypes = new ImageHandler().mimeTypes; 
        $(metadata).children("autnresponse")
            .children("responsedata")
            .children("autn\\:hit")
            .children("autn\\:content")
            .children("DOCUMENT")
            .each(function (index, document) {
                var metadata = {};
                var doc = $(document);
                metadata.uuid = doc.children("DREREFERENCE").text( );
                metadata.updateDate = doc.children("GEONET_UPDATED_DATE").text();
                metadata.title = doc.children("DRETITLE").text();
                metadata.abstract = doc.children("GEONET_ABSTRACT").text();
                var topics = [];
                doc.children("GEONET_TOPIC").each(function (index, topic) {
                    topics.push($(topic).text());
                });
                metadata.topics = topics;
                metadata.linkages = [];

                /***
                * @author - itterates through the linkages XML and generates the correct linkages URL for the images to be displayed
                */
                //TODO: Add code to grab the linkages from the sent XML file and add them to the linkages array which will be used later
                
                var theLink;
                var resourceNum = 1;

                while (true) {
                    // Check to see if there is (or any more) resources left to process
                    if(doc.children("GEONET_TRANSFER_PROTOCOL_" + resourceNum).length == 0) {
                        break;
                    }
                    else {
                        // Process the links that are available from the xml document
                        // Final link string format: name|description|URL|protocol|mime-type
                        theLink = doc.children("GEONET_TRANSFER_NAME_" + resourceNum).text() + "|";
                        theLink += doc.children("GEONET_TRANSFER_DESCRIPTION_" + resourceNum).text() + "|";
                        theLink += doc.children("GEONET_TRANSFER_URL_" + resourceNum).text() + "|";
                        theLink += doc.children("GEONET_TRANSFER_PROTOCOL_" + resourceNum).text() + "|";



                        // we need to do some wizardry to get the mime-type since the respose doesn't give us this
                        var fileType = /(?:\.([^.]+))?$/.exec(doc.children("GEONET_TRANSFER_URL_" + resourceNum).text())[1];
                        if (fileType != null) {
                            $(mimeTypes).each(function (index, mime) {
                                if (mime.type.indexOf(fileType) > -1)
                                    theLink += mime.type;
                            });
                        }
                        else {
                            theLink += doc.children("GEONET_TRANSFER_PROTOCOL_" + resourceNum).text();
                        }

                        theLink += "|" + metadata.uuid;

                        metadata.linkages.push(theLink);

                        resourceNum++;
                    }
                }
                
                results.push(metadata);
            });
        return results;
    }
}

/*
 * Utility renderer that will handle the latest feed on the application
 * home page.
 */
function IdolLatestSearchRenderer(state) {
    var self = this;

    IdolSearchRenderer.call(self, state);

    self.renderSummarySections = function (data) { }

    self.updateCount = function (data) { }

    self.setupPage = function () { }

    self.renderMetadataSections = function (data) {
        var container = $("#posts-latest");
        var metadata = self.buildMetadataStructure(data);
        var properties = ["title", "abstract"];

        container.remove(".item");
        //$.parseXML()
        $(metadata).each(function (index, metadata) {
            var itemDiv = $("<div class='item'>").appendTo(container);
            $("<div class='image'><img src='../../images/topic_icons/default-new-releases.png' /></div>").appendTo(itemDiv);
            var bodyDiv = $("<div class='body'>").appendTo(itemDiv);
            for (var x = 0; x < properties.length; x++) {
                if (properties[x] == "title") {
                    var titleDiv = $("<div class='" + properties[x] + "'>").appendTo(bodyDiv);
                    var uuid = metadata.uuid;
                    var link = $("<a>").attr("href", "#" + uuid).html(metadata.title);
                    link.click(function (e) {
                        e.preventDefault();
                        var id = $(e.currentTarget).attr('href').replace("#", "");
                        tabs.selectTab($('a[href$="tab-search"]'));
                        tabs.resultTab.showSingleResult(id);

                    });
                    titleDiv.append(link);
                    bodyDiv.append(titleDiv);
                } else {
                    var feedString = "<div class='" + properties[x] + "'>";
                    feedString += self.shortenText(metadata[properties[x]], 200);
                    feedString += "</div>";
                    bodyDiv.append($(feedString));
                }
            }
            //TODO: LISAsoft wd - we must fix image icons currently just set to default
        });
    }
}

/*
 * Utility renderer that will handle the right-hand feeds on the application
 * home page.
 */
function IdolFeedRenderer(state, jquerySelIn, propertiesIn) {
    var self = this;
    var jquerySel = jquerySelIn;
    var properties = propertiesIn;

    IdolSearchRenderer.call(self, state);

    self.renderSummarySections = function (data) { }

    self.updateCount = function (data) { }

    self.setupPage = function (data) { }

    self.renderMetadataSections = function (data) {
        var metadata = self.buildMetadataStructure(data);
        $(jquerySel).remove(".item");
        //$.parseXML()
        $(metadata).each(function (index, metadata) {
            var itemDiv = $("<div class='item'>").appendTo($(jquerySel));
            //var feedString = "<div class='item'>";
            for (var x = 0; x < properties.length; x++) {
                if (properties[x] == "title") {
                    var titleDiv = $("<div class='" + properties[x] + "'>").appendTo(itemDiv);
                    var uuid = metadata.uuid;
                    var link = $("<a>").attr("href", "#" + uuid).html(metadata.title);
                    link.click(function (e) {
                        e.preventDefault();
                        var id = $(e.currentTarget).attr('href').replace("#", "");
                        tabs.selectTab($('a[href$="tab-search"]'));
                        tabs.resultTab.showSingleResult(id);
                    });
                    titleDiv.append(link);
                    itemDiv.append(titleDiv);
                    // WD: changed  updateDate to changeDate
                } else if (properties[x] === "updateDate") {
                    var updateDate = metadata.updateDate;
                    if (!updateDate) {
                        updateDate = '';
                    }
                    var feedString = "<div class='updateDate'>" + updateDate + "</div>";
                    itemDiv.append($(feedString));
                } else {
                    var feedString = "<div class='" + properties[x] + "'>";
                    feedString += self.shortenText(metadata[properties[x]], 250);
                    feedString += "</div>";
                    itemDiv.append($(feedString));
                }
            }
        });
    }
}