﻿
/*
 * Implements the Lucene specific data query-line generation for the search state.
 */
function LuceneSearchState() {
    var self = this;

    SearchState.call(self);
    /*
     * Because the handling of array values by jQuery is different than what GeoNetwork expects,
     * we actually have to make this a string ourselves.
     */
    self.getSearchData = function () {
        var string = "request=search&fast=index";
        /*
        var data = {
            request: 'search',
            fast: 'index',
        };
        */
        var hits = parseInt(self.searchData['hitsperpage']);
        // We're using 0-based internally, but the request must be 1-based
        var page = parseInt(self.searchData['page']);
        var from = (page * hits) + 1;
        var to = (from + hits) - 1;
        string = string + "&hitsperpage=" + hits;
        string = string + "&from=" + from;
        string = string + "&to=" + to;
        if (self.sortBy != '') {
            string = string + "&sortBy=" + self.sortBy;
        }
        if (self.sortOrder != '') {
            string = string + "&sortOrder=" + self.sortOrder;
        }
        /*
        data['hitsperpage'] = hits;
        data['from'] = from;
        data['to'] = to;
        */
        jQuery.each(self.searchTerms, function (key, term) {
            /*
        $(Object.keys(self.searchTerms)).each(function (index, key) {
            var term = self.searchTerms[key];
            */
            if (term != null && term != '') {
                if ($.isArray(term)) {
                    $(term).each(function (index, value) {
                        string = string + "&" + key + "=" + value;
                    });
                } else {
                    string = string + "&" + key + "=" + term;
                }
                /*
                data[key] = term;
                */
            }
        });

        if (!isNaN(self.extents.xmin) && !isNaN(self.extents.ymin) && !isNaN(self.extents.xmax) && !isNaN(self.extents.ymax)) {
            string = string + "&westBL=" + self.extents.xmin;
            string = string + "&southBL=" + self.extents.ymin;
            string = string + "&eastBL=" + self.extents.xmax;
            string = string + "&northBL=" + self.extents.ymax;
        }

        return string;
    }

    self.executeSearch = function (searchRenderer) {
        var searchData = self.getSearchData();
        this.searchRenderer = searchRenderer;
        $.get("../../ProxyHandler.ashx", searchData, function (data, textStatus, jqXHR) {
            var data1 = $.parseXML(data);
            self.searchRenderer.setupPage();
            searchRenderer.renderMetadataSections(data1);
            searchRenderer.renderSummarySections(data1);
            var count = $(data1).find('summary').attr("count");
            searchRenderer.updateCount(count);

        }).fail(function (data, textStatus, jqXHR) {
            console.log("Search failed. " + textStatus + data + jqXHR);
        });
    }
}
