﻿var search;

/*
 * Responsible for handling the activation of the search tab.  Provides
 * convenience methods for initiating searches.
 */
function SearchTab() {
    var self = this;
    //var searchState = new LuceneSearchState();
    //var renderer = new LuceneSearchRenderer(searchState);

    var searchState = new IdolSearchState();
    var renderer = new IdolSearchRenderer(searchState);

    var hasRun = false;

    self.clearSearch = function () {
        searchState.clear();
    }

    self.getSearchState = function () {
        return searchState;
    }

    self.showSearch = function () {
        tab = $("ul.navHead a[href=#tab-search]");
        tabs.selectTab(tab);
    }

    /*
     * This clears the search state and runs a new search using the provided full-text string.
     */
    self.anySearch = function (anyValue) {
        searchState.clear();
        searchState.addSearchTerm('any', anyValue);
        this.search();
    }

    self.hasRun = function () {
        return hasRun;
    }

    self.search = function (refreshRefinement) {
        hasRun = true;
        searchState.executeSearch(renderer, refreshRefinement);
    }


    self.testDateBlocks = function () {
        var testDates = [
            new Date("7 May 2014"),
            new Date("30 November 2014"),
            new Date("4 June 2016"),
            new Date("1 January 2016"),
            new Date("26 October 2022")
        ];
        $(testDates).each(function (index, date) {
            console.log(date);
            var ranges = self.getDateBlocks(date);
            console.log(ranges);
        });
    }
}




