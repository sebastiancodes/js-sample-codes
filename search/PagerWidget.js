﻿/*
 * Builds the widgets needed to change the per-page count and to navigate 
 * pages.  Also responsible for updating the SearchState with the current
 * page and per-page values.
 */
function PagerWidget(state, renderer) {
    var self = this;
    var searchRenderer = renderer;
    var searchState = state;

    self.buildWidget = function (count) {
        /* We'll handle page as per display, so one-based. State is zero-based for math. */
        var currPage = state.getPage() + 1;
        var hitsPerPage = state.getHitsPerPage();
        var maxPage = Math.ceil(count / hitsPerPage);
        if (maxPage <= 1) return "";
        var baseDiv = $('<div>');
        baseDiv.append(self.buildPageSelect(hitsPerPage));

        var pages = self.generatePageArray(currPage, maxPage);
        var pageSpan = $('<span>').appendTo(baseDiv).addClass('page-select');
        var list = $('<ul>').appendTo(pageSpan);
        var prev = $('<li>').appendTo(list);
        if (currPage != 1) {
            $('<a>').appendTo(prev).attr('href', '#prev').html('<').click(function (e) {
                e.preventDefault();
                searchState.setPage(state.getPage() - 1);
                searchState.executeSearch(renderer);
            });
        } else {
            prev.html('<');
        }
        $(pages).each(function (index, value) {
            var item = $('<li>').appendTo(list);
            var pagenum = value;
            if (pagenum == '...') {
                item.html(pagenum);
            } else if (pagenum == currPage) {
                item.html(pagenum).addClass('page-selected');
            } else {
                var link = $('<a>').appendTo(item).attr('href', '#' + pagenum).html(pagenum).click(function (e) {
                    e.preventDefault();
                    state.setPage(pagenum - 1);
                    searchState.executeSearch(renderer);
                });
            }
        });
        var next = $('<li>').appendTo(list);
        if (currPage != maxPage) {
            $('<a>').appendTo(next).attr('href', '#next').html('>').click(function (e) {
                e.preventDefault();
                state.setPage(state.getPage() + 1);
                searchState.executeSearch(renderer);
            });
        } else {
            next.html('>');
        }
        $('<li>').appendTo(list).html(' ');

        return baseDiv;
    }


    self.buildPageSelect = function (hitsPerPage) {
        var outer = $('<span>').addClass('per-page-select');
        $('<span>').appendTo(outer).html('View ');

        var dd = $('<span>').appendTo(outer).addClass('dropdown').addClass('btn');
        var btn = $('<a>').appendTo(dd).attr('data-toggle', 'dropdown').attr('href', '#').html(hitsPerPage);
        $('<span>').appendTo(btn).addClass('caret');
        
        var list = $('<ul>').appendTo(dd).addClass('dropdown-menu').attr('aria-labelledby', 'dropDownMenu1');
        var optArr = [5, 10, 20, 50];
        $(optArr).each(function (index, value) {
            var li = $('<li>').appendTo(list).attr('role', 'presentation');
            var a = $('<a>').appendTo(li).attr('role', 'menuitem').attr('tabindex', '-1').attr('href', '#').html(value);
            a.click(function (event) {
                event.preventDefault();
                var perPage = $(event.currentTarget).text();
                searchState.setHitsPerPage(perPage);
                searchState.executeSearch(searchRenderer);
            });
        });

        $('<span>').appendTo(outer).html(' per page');
        return outer;
    }

    /*
     * This will return an array of page numbers that will be displayed in the nav, with separator
     * sections as an ellipsis string.
     */
    self.generatePageArray = function (currPage, maxPage) {
        var separator = '...';
        var maxSegments = 7;
        var result = [];
        /* If we can fit all pages in the list */
        if (maxPage <= maxSegments) {
            for (var i = 1; i <= maxPage; i++) {
                result.push(i);
            }
            /* If the current is 'near' the front, such that no separator is needed before it. */
        } else if (currPage <= maxSegments - 2) {
            var after = maxSegments - 2;
            for (var i = 1; i <= after; i++) {
                result.push(i);
            }
            result.push(separator);
            result.push(maxPage);
            /* If the current is 'near' the end, such that no separator is needed after it. */
        } else if (currPage >= maxPage - maxSegments + 2) {
            var before = maxPage - (maxSegments - 2);
            result.push(1);
            result.push(separator);
            for (var i = before; i <= maxPage; i++) {
                result.push(i);
            }
            /* Current is somewhere in the middle, so two separators are needed. */
        } else {
            result.push(1);
            result.push(separator);
            var before = Math.floor((maxSegments - 5) / 2);
            var after = Math.ceil((maxSegments - 5) / 2);
            for (var i = currPage - before; i <= currPage + after; i++) {
                result.push(i);
            }
            result.push(separator);
            result.push(maxPage);
        }
        return result;
    }

    self.test = function () {
        console.log("testing ", 1, 6);
        console.log(self.generatePageArray(1, 6));
        console.log("testing ", 1, 20);
        console.log(self.generatePageArray(1, 20));
        console.log("testing ", 1, 9);
        console.log(self.generatePageArray(1, 9));
        console.log("testing ", 9, 9);
        console.log(self.generatePageArray(9, 9));
        console.log("testing ", 1, 30);
        console.log(self.generatePageArray(1, 30));
        console.log("testing ", 14, 30);
        console.log(self.generatePageArray(14, 30));
        console.log("testing ", 28, 30);
        console.log(self.generatePageArray(28, 30));
        console.log("testing ", 30, 30);
        console.log(self.generatePageArray(30, 30));
        console.log("testing ", 1, 1);
        console.log(self.generatePageArray(1, 1));
    }
}
