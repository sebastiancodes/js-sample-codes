﻿/*
 * Manages the state of the search, including all the search refinements terms,
 * full text search, date ranges, extents, per-page and offsets.  Provides 
 * a single abstract function that calls the appropriate search API and 
 * sends the results to the appropriate methods in the provided search renderer.
 */
function SearchState() {
    var self = this;
    self.dateValue = '';
    self.extents = {
        xmin: NaN,
        xmax: NaN,
        ymin: NaN,
        ymax: NaN
    };

    self.searchTerms = {};
    self.searchData = {
        page: 0,
        hitsperpage: 5
    };
    self.sortBy = '';
    self.sortOrder = '';
    self.setSearchOrder = function (order) {
        self.sortBy = order.value;
        self.sortOrder = order.order;
    }

    /*
     * Abstract function that will perform the execution of the search.
     */
    self.executeSearch = function (searchRenderer) {
        throw "SearchState.executeSearch is abstract and must be extended by a subclass.";
    }

    self.clear = function () {
        self.searchTerms = {};
        self.searchData.page = 0;
        self.clearSearchExtents();
        self.dateValue = '';
    }
    self.getHitsPerPage = function () {
        return parseInt(self.searchData['hitsperpage']);
    }
    self.setHitsPerPage = function (hits) {
        if (!isNaN(hits)) {
            self.searchData['hitsperpage'] = hits;
        }
    }
    self.getPage = function () {
        return parseInt(self.searchData['page']);
    }
    self.setPage = function (page) {
        if (!isNaN(page)) {
            self.searchData['page'] = page;
        }
    }

    self.setSearchExtents = function (xmin, ymin, xmax, ymax) {
        self.extents.xmin = xmin;
        self.extents.xmax = xmax;
        self.extents.ymin = ymin;
        self.extents.ymax = ymax;
    }

    self.clearSearchExtents = function () {
        self.extents = {
            xmin: NaN,
            xmax: NaN,
            ymin: NaN,
            ymax: NaN
        };
        featureGeom = "";
    }

    self.getSearchTerms = function (name) {
        var term = self.searchTerms[name];
        if ($.isArray(term)) {
            return term.join(" ");
        } else {
            return term;
        }
    }

    self.addSearchTerm = function (name, value) {
        if (self.hasSearchTerm(name, value)) {
            return;
        }
        var term = self.searchTerms[name];
        if (!term) {
            self.searchTerms[name] = value;
        } else {
            if ($.isArray(term)) {
                term.push(value)
                self.searchTerms[name] = term;
            } else {
                var newTerm = [];
                newTerm.push(term);
                newTerm.push(value);
                self.searchTerms[name] = newTerm;
            }
        }
    }

    self.hasSearchTerm = function (name, value) {
        var term = self.searchTerms[name];
        if (!term) {
            return false;
        }
        if ($.isArray(term)) {
            var found = false;
            $(term).each(function (index, t) {
                if (t == value) {
                    found = true;
                    return false;
                }
            });
            return found;
        } else {
            return term == value;
        }
    }

    /*
     * Removes all search terms defined against the given attribute,
     */
    self.removeSearchTerms = function (name) {
        self.searchTerms[name] = null;
    }

    /*
     * Removes the specified search term.  This is multi-safe, in that it will
     * only remove the given term from the search attribute.  To remove all values
     * of the attribute, use removeSearchTerms(name)
     */
    self.removeSearchTerm = function (name, value) {
        var term = self.searchTerms[name];
        if (term) {
            if ($.isArray(term)) {
                var i = $.inArray(value, term);
                if (i != -1) {
                    term.splice(i, 1);
                    if (term.length == 1) {
                        self.searchTerms[name] = term[0];
                    } else {
                        self.searchTerms[name] = term;
                    }
                }

            } else {
                if (term == value) {
                    self.searchTerms[name] = null;
                }
            }
        }
    }

    self.test = function () {
        self.searchTerms = {
        };
        console.log("empty", self.searchTerms);
        self.addSearchTerm("blah", "ab");
        console.log("add", self.searchTerms);
        self.addSearchTerm("blah", "abc");
        console.log("add", self.searchTerms);
        self.addSearchTerm("blah", "abcd");
        console.log("add", self.searchTerms);
        self.removeSearchTerm("blah", "dcebm");
        console.log("bad remove", self.searchTerms);
        self.removeSearchTerm("blah", "abc");
        console.log("remove", self.searchTerms);
        self.removeSearchTerm("blah", "ab");
        console.log("remove", self.searchTerms);
        self.removeSearchTerm("blah", "ab");
        console.log("bad remove", self.searchTerms);
        self.removeSearchTerm("blah", "abcd");
        console.log("remove", self.searchTerms);
    }
}
