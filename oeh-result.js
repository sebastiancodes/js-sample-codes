﻿/*
 * Because of esri's requirement for DOJO instead of jQuery, the MapVisualiser is housed in the oeh-location javascript, not here.
 */

$(document).ready(function () {

    /* 15904  10/04/2015 activate enter event on sreach field */
    $("#tab-result .search-field").keypress(function (e) {
        if (e.which == 13) {
            var searchVal = $("#tab-result .search-field").prop("value");
            tabs.selectTab($('a[href$="tab-search"]'));
            $('.search-field').val(searchVal); // 10/04/2015 Clear the search fields
            tabs.searchTab.anySearch(searchVal);
        }
    });
    
    $("#tab-result .search-button").click(function (e) {
        var searchVal = $("#tab-result .search-field").prop("value");
        tabs.selectTab($('a[href$="tab-search"]'));
        $('.search-field').val(searchVal); // 9/04/2015 Clear the search fields
        tabs.searchTab.anySearch(searchVal);

    });
});

/*
 * Responsible for fetching a metadata record and laying out the results
 * that GeoNetwork returns.
 */
function ResultTab() {
    var self = this;

    self.showSingleResult = function (uuid) {        
        //var dataTab = $('*[href="#tab-data"]')[0];
        //tabs.selectTab(dataTab);
        $('.tab-show').removeClass('tab-show');
        $("#tab-result").addClass('tab-show');
        dataTabs.selectTab($('#data-tabs a[href=#result-container]'));
        $('#data-tabs a[href=#visualise-this]').parent().hide();
        $('#legend-container').hide();
        var container = $("#result-container");
        container.empty();
        $("<img>").appendTo(container).attr("src", "../../Images/spinner.gif");

        var data = { "request": "metadata", "currTab": "openOEHPublic", "uuid": uuid };

        $.get("../../ProxyHandler.ashx", data, this.renderResults).fail(function (data, textStatus, jqXHR) {
            var container = $("#result-container");
            container.empty();
            container.html("<h4>Unable to load metadata</h4>");
            console.error("Failure loading result. " + textStatus + "-" + data);
        });
    }

    /*
     * This is called to handle a successful load of the metadata. 
     * As well as actually adding the results to the page, it adds visual elements that I didn't trust the xslt with.
     */
    self.renderResults = function (data, textStatus, jqXHR) {
        var container = $("#result-container");
        container.empty();
        container.html(data);

        // Add format images
        var ih = new ImageHandler();
        $(".data table td:first-child").each(function (index, item) {
            ih.createLargeImageTag(item);
        });

        // Add in table spacer rules
        $("<tr>").append($("<td colspan='4'>").append($("<hr>"))).insertAfter($(".data table tr:not(:last-child)"));
        $("<tr>").append($("<td colspan='2'>").append($("<hr>"))).insertAfter($(".additional table tr:not(:last-child)"));

        // Rewrite links
        $(".data td.download a").each(function (index, element) {
            var ep = $(element).parent();
            var size = ep.children("span.source-size").text();
            var link = ep.children("span.link").text();
            var type = ep.children("span.file-type").text();
            ep.empty();

            if (size == null || size == "") size = cart.unknownFileSize;
            var data = {
                "size": size,
                "filelink": link,
                "type": type
            };

            $("<a>").appendTo(ep).click(data, function (e) {
                e.preventDefault();
                //var metadataId = $("div .add-name:contains('Unique Identifier')").next().text();
                //cart.fetch(metadataId);
                
                var metadataId = $("div .add-name:contains('Unique Identifier')").next().text();
                var metadataTitle = $("div .metadata-title").text();
                if (typeof metadataTitle.trim !== 'function') {
                    metadataTitle = metadataTitle.replace(/^\s+|\s+$/g, '');
                } else {
                    metadataTitle = metadataTitle.trim();
                }
                metadataTitle = metadataTitle.replace("\n", "");
                var dataset = new Dataset(metadataId, metadataTitle,
                "test", [
                    new DatasetItem(e.data.type, e.data.filelink, size)
                ]);
                cart.packageAndFetch(dataset);
                
            }).html("Download");
            $("<span> | </span>").appendTo(ep);
            
            $("<a>").appendTo(ep).click(data, function (e) {
                e.preventDefault();

                var metadataId = $("div .add-name:contains('Unique Identifier')").next().text();
                var metadataTitle = $("div .metadata-title").text();
                if (typeof metadataTitle.trim !== 'function') {
                    metadataTitle = metadataTitle.replace(/^\s+|\s+$/g, '');
                } else {
                    metadataTitle = metadataTitle.trim();
                }
                metadataTitle = metadataTitle.replace("\n","");

                var dataset = new Dataset(metadataId, metadataTitle,
                "test", [
                    new DatasetItem(e.data.type, e.data.filelink, size)
                ]);
                dataset.save();
                getCart();

            }).html("Add to Cart");
        });

        $(".data td.service a").each(function (index, element) {
            var lnk = $(element).attr('href');

            var e = $(element).parent();

            e.empty();
            $("<img>").appendTo(e).attr('src', '../../Images/extlink.png')
                .attr('alt', 'External link')
                .attr('style', "width: 15px; height: 16px");
            $("<a>").appendTo(e).html("Connect").attr('href', lnk).attr('target', '_blank');
        });

        /* Move the tag-cloud items */
        $('#tag-cloud').empty();
        $('.metadata-tag-cloud').appendTo($('#tag-cloud'));

        // Shorten the abstract
        $(".metadata-abstract").each(function(index, element) {
            self.shortenSection(element);
        });

        /* Find any links that can be used for visualisation */
        mapVisualiser.clearLayers();
        var restLink = $('.filetype-rest').parent().find('.service a').attr('href');
        mapVisualiser.addRest(restLink);        
        var wmsLink = $('.filetype-wms').parent().find('.service a').attr('href');
        mapVisualiser.addWMS(wmsLink);
        

        if (mapVisualiser.hasLayer()) {
            $('#data-tabs a[href=#visualise-this]').parent().show();
        } else {
            $('#data-tabs a[href=#visualise-this]').parent().hide();
        }

        /* SOW 4/4/2015 -  adding Support page*/
        $(".metadata-support-links").each(function (index, element) {
            var metadataId = $("div .add-name:contains('Unique Identifier')").next().text();
            var metadataTitle = $("div .metadata-title").text();
            /*$("<a>").appendTo(element).attr("href", "#tab-support").html(" Request Data ");*/
            $("<a>").appendTo(element).attr("href", "#tab-support").html(" Request Data ").click(function () {
                $("#msg-data-title").val(metadataTitle);
                $("#msg-type").val("1");
                $("#msg-type").trigger("change");
                tabs.selectTab($(this));
            });
            $("<span> | </span>").appendTo(element);
            $("<a>").appendTo(element).attr("href", "#tab-support").html(" Feedback ").click(function () {
                $("#msg-data-title").val(metadataTitle);
                $("#msg-type").val("2");
                $("#msg-type").trigger("change");
                tabs.selectTab($(this));
            });
        });

    }

    /*
     * There are two things the happen here.  We shorten the section, adding in a 'Read more' link.  We also replace 
     * newline characters using the replaceNewlines method.  Shortening uses the util method, TruncateString.
     *

     */
    self.shortenSection = function (element, newCharLength) {
        var charLength = (newCharLength ==null) ?1000 : newCharLength;

        if($(element).children().length != 0) {
            return;
        }

        //var fullBody = $(element).html().outerHtml().trim();
        var fullBody = $(element).get(0).outerHTML;
        if (fullBody != "undefined" && fullBody.length <= charLength) {
            return;
        }

        shortBody = TruncateString(fullBody, charLength);

        // Swap in some breaks.
        fullBody = this.replaceNewlines(fullBody);
        shortBody = this.replaceNewlines(shortBody);
        
        // Now we build dom.
        $(element).empty();
        var shortDiv = $("<div>").appendTo($(element)).html(shortBody);
        var fullDiv = $("<div>").appendTo($(element)).html(fullBody).hide();

        $("<a href='#readmore'>").appendTo(shortDiv).html("Read more").click(function(e) {
            e.preventDefault();
            shortDiv.hide();
            fullDiv.show();
        });
        $("<a href='#readless'>").appendTo(fullDiv).html("Read less").click(function(e) {
            e.preventDefault();
            fullDiv.hide();
            shortDiv.show();
        });        
    }

    self.replaceNewlines = function (text) {
        var newlineArray = [0];
        var index = text.indexOf("\n");
        while (index != -1) {
            newlineArray.push(index);
            index = text.indexOf("\n", index + 1);
        }
        newlineArray.push(text.length + 1);
        var newText = "";
        for (var i = 1; i < newlineArray.length; i++) {
            newText = newText + "<p>" + text.substring(newlineArray[i - 1], newlineArray[i]) + "</p>";
        }
        return newText;
    }
}
