These JavaScript files are part of an Open Data Portal application 
written in .Net environment using JavaScript and JQuery libraries. 
The goal of this project is to handle and present XML responses from 
IDOL Search engine or GeoNetwork as a Web Service, and provide 
download functionality for dataset resources attached to a metadata.


﻿Search Engine Changes
*********************

There are a number of files that will need to be changed when switching between
the Lucene and Idol searches:

 * SearchTab must change the state and renderers it initialises.
 * oeh-data has three initialising functions that each use a search state
   and renderer.  These need to be update (and implemented ATM).

Overview of JavaScript
**********************

oeh-cart.js
===========

Responsible for the cart functionality, including objects for maintaining 
and updating the current state of the cart and related helper methods.

oeh-data.js
===========

Responsible for the data tab, or the applications home tab.  This includes 
the three feeds, the carosel and the main search box.

oeh-imagehandler.js
===================

Provides functionality to map between the protocol and mime types supplied
by GeoNetwork, and the human-readable format names displayed by the application.

oeh-location.js
===============

This file is responsible for all the ESRI-integrated code.  This includes both
the location search tab in the main navigation (MapHandler object) and the 
visualise this tab (MapVisualiser object) in the results page.  This should 
be the only file that uses Dojo functionality instead of jQuery.  It also
includes the GazetteerHandler object used to search locations on the location 
search tab.

oeh-nav.js
==========

Provides the activation functionality for the tabbed navigation.  It also has
hooks into some of the tab objects to allow fancier functionality, such as 
activating the results tab, and searching when the search tab is selected 
for the first time.

oeh-result.js
=============

Contains handlers for the ResultTab, with the exception the the visualise this 
tab.

oeh-util.js
===========

Provides a handful of generally useful methods for string truncation and date
formating and such.

Search Page Objects
===================

Due to the need to refactor the search page to allow the use of both the Lucene
and Idol searches (to hedge our bets), the search related objects have been
broken out into individual files.

DateSelector.js
---------------

Generates the date ranges used for filtering and sets up the selection box
and its hooks.

PagerWidget.js
--------------

Responsible for building the per-page selector and the page links, as well as 
updating the search state with the current page details.

SearchRenderer.js
-----------------

Responsible for rendering the details of the search into the page.  Contains
three 'abstract' methods to be provided by the implementation-specific 
subclasses: buildSummaryStructures, buildMetadataStructures and getSearchByData.
These classes convert the search engine specific response into json that can be 
rendered by the parent class.  There are two implementions of the search renderer:

 * IdolSearchRenderer.js
 * LuceneSearchRenderer.js

SearchState.js
--------------

Provides functions to manage the state of the search and execute the search.
The search execution is handled by subclasses, which require specific knowledge 
regarding the search string format and endpoints.  There are two implementions
of the search state:

 * IdolSearchState.js
 * LuceneSearchState.js

SearchTab.js
------------

Handles the activation and switching of the search tab, as well as initialisation
of the state and renderer components.

SortSelector.js
---------------

Generates the widget for the search sort and sets hooks into the search state.

TopicMapper.js
--------------

Provides mapping functions between the machine readable names of topics, and the
human readable ones.  In reality, this just shortens the names so they fit on the page.