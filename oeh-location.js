﻿/*
 * The ArcGIS web toolkit uses Dojo for its magic, while the standard OEH libraries include 
 * jQuery instead.  So I'm sorry, but this file is mostly dojo.  In order to keep things 
 * somewhat sane, let's try and keep jQuery out of here, and dojo out of everywhere else.
 */
esriConfig.defaults.io.proxyUrl = "../../Proxy.ashx"
esriConfig.defaults.io.alwaysUseProxy = false;

var mapHandler = new MapHandler();
var featureGeom = "";
/*
 * Handler for the mapping functionality of the location search tab.
 */
function MapHandler() {
    dojo.require("dijit.layout.BorderContainer");
    dojo.require("dijit.layout.ContentPane");
    dojo.require("dijit.Dialog");
    dojo.require("dijit.form.CheckBox");
    dojo.require("dijit.form.Form");
    dojo.require("esri.map");

    var self = this;
    var searchState;
    var searchTab;
    var gazHandler = new GazetteerHandler();

    var map, drawToolbar;//, resizeTimer, infoTemplate;
    var isInitialised = false;
    var xmin = NaN;
    var ymin = NaN;
    var xmax = NaN;
    var ymax = NaN;

    self.init = function() {
        if (!isInitialised) {
            self.createMap();
            if (featureGeom) {
                // wait for map to load
                // add event listener for map.onLoad
                //self.enableExtentTool();
                dojo.connect(map, "onLoad", function () {
                    addToMap(featureGeom);
                });
            }
            else {
                xmin = NaN;
                ymin = NaN;
                xmax = NaN;
                ymax = NaN;
            }
        }
        searchTab = tabs.searchTab;
        searchState = searchTab.getSearchState();
        
        var candidates = gazHandler.getCandidates();
        $('#location-gazetteer').autocomplete({ source: candidates });


        var sum = $("#search-summary-location").empty();
        var currSearch = "All metadata records";
        if (searchState.getSearchTerms('any') != "" && searchState.getSearchTerms('any') != undefined) {
            currSearch = searchState.getSearchTerms('any');
        } else if (searchState.getSearchTerms('Geonet_Topic_Filter') != "" && searchState.getSearchTerms('Geonet_Topic_Filter') != undefined) {
            currSearch = searchState.getSearchTerms('Geonet_Topic_Filter');
        }
        $("<span>").appendTo(sum).html(currSearch);
    }

    self.clearBounds = function () {
        searchState.clearSearchExtents();
        map.graphics.clear();
        xmin = NaN;
        ymin = NaN;
        xmax = NaN;
        ymax = NaN;
        featureGeom = "";
    }

    self.executeSearch = function () {
        searchState.setSearchExtents(xmin, ymin, xmax, ymax);
        searchTab.search();
        searchTab.showSearch();
    }

    self.acceptGazetteerSelection = function (a) {
        var gazName = $('#location-gazetteer').val();
        var e = gazHandler.getLocation(gazName);
        var extent;
        if (e !== undefined) {
            require([
                    "esri/geometry/Extent", "esri/SpatialReference",
                ], function(Extent, SpatialReference) {
                    extent = new Extent(e.xmin, e.ymin, e.xmax, e.ymax, new SpatialReference({ "wkid": 4283 }));
            });
            self.clearBounds();
            addToMap(extent);
        }
    }

    self.enableExtentTool = function () {
        require(["dojo/dom-class"], function (domClass) {
            domClass.add("extentTool", "active-button");
        });
        self.clearBounds();
        drawToolbar = new esri.toolbars.Draw(map);
        drawToolbar.activate(esri.toolbars.Draw.EXTENT);
        map.setMapCursor('crosshair');
        dojo.connect(drawToolbar, "onDrawEnd", addToMap);
    }
    
    function addToMap(geometry) {
        featureGeom = geometry;

        var symbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([214, 106, 5]), 2), new dojo.Color([214, 106, 5, 0.25]));
        
        var graphic = new esri.Graphic(geometry, symbol, {
            extentjson: dojo.toJson(geometry.toJson()),
            extent: geometry.xmin + "," + geometry.ymin + "," + geometry.xmax + "," + geometry.ymax
        });        
        map.graphics.add(graphic);
        
        if (drawToolbar !== undefined) {
            drawToolbar.deactivate();

        }
        require(["dojo/dom-class"], function (domClass) {
            domClass.remove("extentTool", "active-button");
        });
        map.setMapCursor("default");
        xmin = geometry.xmin;
        ymin = geometry.ymin;
        xmax = geometry.xmax;
        ymax = geometry.ymax;
        var extent;
        require(["esri/geometry/Extent", "esri/SpatialReference"], function(Extent, SpatialReference) {
            extent = new Extent(xmin,ymin,xmax,ymax, new SpatialReference({ wkid:4283 }));
        });
        //self.setMapExtent(extent);
    }    

    self.createMap = function() {
        //isInitialised = true;
        $("#map-search").empty();
        require(["esri/map", "esri/layers/ArcGISTiledMapServiceLayer", "dojo/domReady!"],
              function (Map, Tiled) {
                  map = new esri.Map("map-search", { extent: esri.geometry.Extent(141, -38, 151, -28, new esri.SpatialReference({ "wkid": 4283 })) });
                  //map = new esri.Map("map-search", { extent: esri.geometry.Extent(129, -47, 173, -18, new esri.SpatialReference({ "wkid": 4283 })) });
                  var tiled = new Tiled("http://appmapdata.environment.nsw.gov.au/arcgiswa/rest/services/Basemap/Basemap_Geonetwork/MapServer");
                  map.addLayer(tiled);
                  self.connectMap();
              }
            );
    }

    self.connectMap = function() {
        //dojo.connect(map, "onLoad", createExtentMarker);
        dojo.connect(map, 'onLoad', function (theMap) {
            dojo.connect(dijit.byId('map-search'), 'resize', function () {
                clearTimeout(resizeTimer);
                resizeTimer = setTimeout(function () {
                    map.resize();
                    map.reposition();
                }, 500);
            });
        });
    }

    self.setMapExtent = function (extent) {
        if (extent.spatialReference.wkid == 4283) {
            map.setExtent(extent);
            return;
        }
    }    
}

var mapVisualiser = new MapVisualiser();

/*
 * Handler for the map functionality used by the 'visualise this' tab of
 * the results page.
 */
function MapVisualiser() {
    dojo.require("dijit.layout.BorderContainer");
    dojo.require("dijit.layout.ContentPane");
    dojo.require("dijit.Dialog");
    dojo.require("dijit.form.CheckBox");
    dojo.require("dijit.form.Form");
    dojo.require("esri.map");
    dojo.require("esri.geometry");

    var self = this;

    var map;//, resizeTimer, infoTemplate;

    var isInitialised = false;
    var xmin = NaN;
    var ymin = NaN;
    var xmax = NaN;
    var ymax = NaN;

    self.init = function () {
        if (!isInitialised) {
            self.createMap();
        }
        self.refreshMap();
    }

    self.getMap = function () {
        return map;
    }
    self.setMap = function (newMap) {
        map = newMap;
    }

    var defaultExtent;
    var legend;
    self.createMap = function () {
        isInitialised = true;

        require(["esri/map", "esri/layers/ArcGISTiledMapServiceLayer", "dojo/domReady!"],
        function (Map, Tiled) {
            defaultExtent = esri.geometry.Extent(141, -38, 151, -28, new esri.SpatialReference({ "wkid": 4283 }))
            var map = new esri.Map("map-visualise", {
                extent: defaultExtent,
                autoResize: false
            });
            self.setMap(map);
            var tiled = new Tiled("http://appmapdata.environment.nsw.gov.au/arcgiswa/rest/services/Basemap/Basemap_Geonetwork/MapServer");
            map.addLayer(tiled);
            self.connectMap();
            map.on('layer-add-result', function (lyr) {
                if (legend === undefined) {
                    require(["esri/dijit/Legend"], function (Legend) {
                        var legendLayers = []
                        legendLayers.push({ layer: lyr.layer });
                        legend = new Legend({ map: map, layerInfos: legendLayers }, "legendDiv");
                        legend.startup();
                    });
                } else {
                    var legendLayers = []
                    legendLayers.push({ layer: lyr.layer });
                    legend.refresh(legendLayers);
                }
            });
        });
    }

    self.setMapExtent = function (extent) {
        if(extent && extent.spatialReference.wkid == 4283) {
            map.setExtent(extent);
            return;
        }
    }


    self.refreshMap = function () {
        var layer;
        
        if (restLayer !== undefined) {
            layer = restLayer;
        } else if (wmsLayer !== undefined) {
            layer = wmsLayer;
        }
        if (layer === undefined) {
            return;
        }
        layer.on('error', function (error) {
            showDialog("Error loading map data.");
            dataTabs.selectTab($('#data-tabs a[href=#result-container]'));
            $('#data-tabs a[href=#visualise-this]').parent().hide();
        });
        var extents = defaultExtent;
        if (layer.fullExtent !== undefined) {
            extents = layer.fullExtent;
        }
        map.addLayer(layer);
        self.setMapExtent(extents, true);
        $("#legend-container").show();
    }
    
    self.connectMap = function () {
        dojo.connect(map, 'onLoad', function (theMap) {
            dojo.connect(dijit.byId('map-visualise'), 'resize', function () {
                clearTimeout(resizeTimer);
                resizeTimer = setTimeout(function () {
                    theMap.resize();
                    theMap.reposition();
                }, 500);
            });
        });
    }

    self.clearLayers = function () {
        hasLayer = false;
        if (wmsLayer !== undefined) {
            if (map) {
                map.removeLayer(wmsLayer);
            }
            wmsLayer = undefined;
        }
        if (restLayer !== undefined) {
            if (map) {
                map.removeLayer(restLayer);
            }
            restLayer = undefined;
        }
    }

    var wmsLayer;
    var hasLayer = false;
    self.addWMS = function (url) {
        hasLayer = false;
        if (wmsLayer !== undefined && map !== undefined) {
            map.removeLayer(wmsLayer);
        }
        if (url !== undefined) {
            var arr = url.split('?');
            if (arr.length == 2) {
                var path = arr[0];
                var kvp = arr[1].split('&');
                var layer;
                $(kvp).each(function (i, v) {
                    var p = v.split('=');
                    if (p.length == 2 && p[0].toLowerCase() == 'layers') {
                        layer = p[1];
                        return false;
                    }
                });
                if (layer !== undefined) {
                    try {
                        hasLayer = true;
                        require(["esri/layers/WMSLayer"], function (WMSLayer) {
                            wmsLayer = new WMSLayer(path, { visibleLayers: [layer] });
                        });                                                
                    } catch (e) {
                        hasLayer = false;
                        console.warn("Error creating WMS layer from url: " + url);
                        console.warn(e);
                    }
                } else {
                    console.log("WMS layer names could not be located: " + url);
                }
            } else {
                console.log("WMS url could not be split into host/query: " + url);
            }
        }
    }

    var restLayer;
    self.addRest = function (url) {
        if (restLayer !== undefined && map !== undefined) {
            map.removeLayer(restLayer);
        }
        if (url !== undefined) {
            try {
                hasLayer = true;
                require(["esri/layers/ArcGISDynamicMapServiceLayer"], function (ArcGISDynamicMapServiceLayer) {
                    restLayer = new ArcGISDynamicMapServiceLayer(url);
                });
            } catch (e) {
                hasLayer = false;
                console.warn("Error creating REST layer from url: " + url);
                console.warn(e);
            }
        }
    }

    self.hasLayer = function () {
        return hasLayer;
    }
}

/*
 * Sets up the suggest-as-you-type information used by the location name
 * search field.  This was intended as a place holder for more dynamic
 * functionality, but that never materialised.
 */
function GazetteerHandler() {
    var self = this;
    var places = [];
    var locations = {};
    /*
    <info>
    <regions count="288">
    <region id="http://geonetwork-opensource.org/regions#2" categoryId="http://geonetwork-opensource.org/regions#country" hasGeom="false">
    <id>http://geonetwork-opensource.org/regions#2</id>
    <north>38.47198</north>
    <south>29.40611</south>
    <west>60.50417</west>
    <east>74.91574</east>
    <label><fre>Afghanistan</fre><ara>Afghanistan</ara>
        <eng>Afghanistan</eng><spa>Afghanistan</spa>
    </label>
    <categoryLabel><cat>Country</cat><ger>Land</ger><por>Country</por><tur>Country</tur><fin>Country</fin><fre>Pays</fre><nor>Country</nor><chi>Country</chi><dut>Country</dut><ita>Paese</ita><rus>Country</rus><ara>Country</ara><pol>Country</pol>
        <eng>Country</eng><spa>Country</spa>
    </categoryLabel></region>
    */
    function handleSuccess(data, textStatus, jqXHR ) {
        var labels = [];
        var values = {};
        $(data).find('region').each(function (i, v) {
            var reg = $(v);
            var val = {};
            val.xmin = reg.find('west').html();
            val.ymin = reg.find('south').html();
            val.xmax = reg.find('east').html();
            val.ymax = reg.find('north').html();

            var label = reg.find('label eng').html();
            labels.push(label);
            values[label] = val;
        });
        places = labels;
        locations = values;
    }

    function handleFailure(jqXHR, textStatus, errorThrown) {
        places = [];
        locations = {};
    }

    /*
     * The request is currently unused.  Some day we may get an actual searchable gazetteer, but
     * for now it's a complete list every time.
     */
    self.getCandidates = function () {
        if (places.length == 0) {
            $.ajax({
                url: "../../ProxyHandler.ashx?type=regions&request=gazetteer",
                async: false,
                success: handleSuccess,
                failure: handleFailure
            });
        }
        return places;
    }

    self.getLocation = function (place) {
        return locations[place];
    }

}
