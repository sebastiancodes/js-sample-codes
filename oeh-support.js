var supportEmail;

function SupportEmail(type, toEmail, subject, name, email, phone, dataTitle, body) {
    self = this;
    self.type = type;
    self.toEmail = toEmail;
    self.subject = subject;
    self.name = name;
    self.email = email;
    self.phone = phone;
    self.dataTitle = dataTitle;
    self.body = body;

    self.toJson = function () {
        var meJson = {};
        meJson.Type = self.type;
        meJson.ToEmail = self.toEmail;
        meJson.Subject = self.subject;
        meJson.Name = self.name;
        meJson.Email = self.email;
        meJson.Phone = self.phone;
        meJson.DataTitle = self.dataTitle;
        meJson.Body = self.body;

        return meJson;
    }

    self.sendEmail = function () {
        var json = this.toJson();
        $.post('../../Home/SendEmail', JSON.stringify(json)).done(function (data, textStatus, jqXHR) {
            console.log("Email was sent successfully.");
            // showDialog("Email was sent successfully.");
            showFeedback();
        }).error(function (data, textStatus, jqXHR) {
            console.log("Unable to send email: " + textStatus);
            showDialog("Unable to send email: " + textStatus);
            showSupportForm();
        });
    }
}

$(document).ready(function () {
    showSupportForm();
    setRequireds();
    setHiddens();
    $("#msg-type").change(function () {
        setRequireds();
        setHiddens();
        $(".support-label").removeClass("error-class-label");
        $(".support-field > input").removeClass("error-class-field").removeClass("input-validation-error");
        $(".support-field > textarea").removeClass("error-class-field").removeClass("input-validation-error");
        $(".support-field > select").removeClass("error-class-field").removeClass("input-validation-error");
        $(this).parent().siblings(".support-label").children("span").text("").hide();
        $(".support-field > span").hide();
    });

    $("#support-form input, textarea").on('keyup keypress blur change paste cut', function () {
        if ($(this).prop("required") && $(this).val() == "") {
            $(this).addClass("error-class-field");
            $(this).parent().siblings().addClass("error-class-label");
            $(this).siblings("span").text(" This field is mandatory ").addClass("error-class-label").show();
        } else {
            $(this).removeClass("error-class-field");
            $(this).parent().siblings().removeClass("error-class-label");
            $(this).siblings("span").hide();
        }
    });

    $("#msg-email").on('keyup keypress blur change paste cut', function () {
        if ($(this).val() != "") {
            if (!isEmail($(this).val())) {
                $(this).addClass("error-class-field");
                $(this).parent().siblings().addClass("error-class-label");
                $(this).parent().siblings(".support-label").children("span").text("*").show();
                $(this).siblings("span").text(" Please enter a valid email address ").addClass("error-class-label").show();
            } else {
                $(this).removeClass("error-class-field");
                $(this).parent().siblings().removeClass("error-class-label");
                $(this).parent().siblings(".support-label").children("span").text("").hide();
                $(this).siblings("span").hide();
            }
        }
    });

    $("#support-form-button").click(function () {
        if (supportFormValidation()) {
            type = $("#msg-type").val();
            toEmail = $("#msg-to-email").val();
            subject = $("#msg-subject").val();
            name = $("#msg-name").val();
            email = $("#msg-email").val();
            phone = $("#msg-phone").val();
            dataTitle = $("#msg-data-title").val();
            body = $("#msg-body").val();

            var supportEmail = new SupportEmail(type, toEmail, subject, name, email, phone, dataTitle, body);
            supportEmail.sendEmail();

        } 
    });

    $("#support-form-reset").click(function () {
        resetForm();
        showSupportForm();
    });
});

function isEmail(email) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(email);
}

function supportFormValidation() {
    var emptyFields = 0;
    var notValidFields = 0;
    var msgTypeDefined = 0;
    $('.support-field input,textarea,select').filter('[required]').each(function (i, requiredField) {
        if ($(requiredField).val() == '') {
            emptyFields++;
            $(this).addClass("error-class-field");
            $(this).parent().siblings().addClass("error-class-label");
            $(this).parent().siblings(".support-label").children("span").text("*").show();
            $(this).siblings("span").text(" This field is mandatory ").addClass("error-class-label").show();
            $("#mandatory-fields-msg").text("Please fix errors above ").show();
        }
    });
    var emailAddress = $("#msg-email").val();
    if (emailAddress != '' && !isEmail(emailAddress)) {
        notValidFields++;
        $("#msg-email").addClass("error-class-field");
        $("#msg-email").parent().siblings().addClass("error-class-label");
        $("#msg-email").siblings("span").text(" Please enter a valid email address ").addClass("error-class-label").show();
        $("#mandatory-fields-msg").text("Please fix errors above ").show();
    }
    var msgType = $("#msg-type").val();
    if (msgType == -1) {
        msgTypeDefined++;
        $("#msg-type").addClass("error-class-field");
        $("#msg-type").parent().siblings().addClass("error-class-label");
        $("#msg-type").siblings("span").text(" Please select a subject ").addClass("error-class-label").show();
        $("#mandatory-fields-msg").text("Please fix errors above ").show();
    }
    if (emptyFields > 0 || notValidFields > 0 || msgTypeDefined > 0) {
        //$("#mandatory-fields-msg").text("Please fix errors above ");
        return 0;
    } else {
        return 1;
    }
}

function setRequireds() {
    var msgType = $("#msg-type").val();
    if (msgType == -1) { // Select one
        $("#msg-dataset-details").hide();
        $("#msg-type").prop('required', true);
        $("#msg-name").prop('required', false);
        $("#msg-email").prop('required', false);
        $("#msg-body").prop('required', false);
        $("#msg-data-title").prop('required', false);
        $("#msg-body").attr("placeholder", "");
    } else if (msgType == 4) { // General Inquiry
        $("#msg-dataset-details").hide();
        $("#msg-type").prop('required', false);
        $("#msg-name").prop('required', true);
        $("#msg-email").prop('required', true);
        $("#msg-body").prop('required', true);
        $("#msg-data-title").prop('required', false);
        $("#msg-body").attr("placeholder", "");
    } else if (msgType == 1) { // Request Data
        $("#msg-dataset-details").show();
        $("#msg-type").prop('required', false);
        $("#msg-name").prop('required', true);
        $("#msg-email").prop('required', true);
        $("#msg-body").prop('required', false);
        $("#msg-data-title").prop('required', true);
        $("#msg-body").attr("placeholder", "Required geographical extent of data, or other requirements");
    } else if (msgType == 2) { // Provide feedback on data or metadata
        $("#msg-dataset-details").show();
        $("#msg-type").prop('required', false);
        $("#msg-name").prop('required', true);
        $("#msg-email").prop('required', true);
        $("#msg-body").prop('required', true);
        $("#msg-data-title").prop('required', true);
        $("#msg-body").attr("placeholder", "Required geographical extent of data, or other requirements");
    } else if (msgType == 3) { // Provide feedback on the website
        $("#msg-dataset-details").hide();
        $("#msg-type").prop('required', false);
        $("#msg-name").prop('required', true);
        $("#msg-email").prop('required', true);
        $("#msg-data-title").prop('required', false);
        $("#msg-body").prop('required', true);
        $("#msg-body").attr("placeholder", "");
    }

    $('.support-field input,textarea,select').each(function () {
        $(this).parent().siblings(".support-label").children("span").text("").hide();
    });
    $('.support-field input,textarea,select').filter('[required]').each(function () {
        $(this).parent().siblings(".support-label").children("span").text("*").show();
    });
    $("#mandatory-fields-msg").hide();
}

function setHiddens() {
    var msgType = $("#msg-type").val();
    if (msgType == "-1") {
        $("#msg-to-email").val("");
        $("#msg-subject").val("");
    } else if (msgType == 4) {
        $("#msg-to-email").val("data.broker@environment.nsw.gov.au");
        $("#msg-subject").val("General inquiry");
    } else if (msgType == 1) {
        $("#msg-to-email").val("data.broker@environment.nsw.gov.au");
        $("#msg-subject").val("Data request");
    } else if (msgType == 2) {
        $("#msg-to-email").val("data.broker@environment.nsw.gov.au");
        $("#msg-subject").val("Data/metadata feedback");
    } else if (msgType == 3) {
        $("#msg-to-email").val("web.support@environment.nsw.gov.au");
        $("#msg-subject").val("Website feedback");
    } 
}

function showFeedback() {
    $("#support-form-feedback").show();
    $("#support-form-initial").hide();
}

function showSupportForm() {
    setHiddens();
    setRequireds();
    $("#support-form-feedback").hide();
    $("#support-form-initial").show();
}

function resetForm() {
    $("#msg-type").val("-1");
    $("#msg-to-email").val("");
    $("#msg-subject").val("");
    $("#msg-name").val("");
    $("#msg-email").val("");
    $("#msg-phone").val("");
    $("#msg-data-title").val("");
    $("#msg-body").val("");
}
