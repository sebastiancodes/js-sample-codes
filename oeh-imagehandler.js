﻿/*
 * Provides functionality to map between the protocol and mime types supplied
 * by GeoNetwork and the human-readable format names displayed by the application.
 */
function ImageHandler() {
    var self = this;

    /*
     * Mapping of the filetype classes to protocols and mime types.  This is required, as the 
     * metadata formatter does not have access to the same mimetype generating code that the 
     * index xslt's do.
     */
    self.filetypeMappings = [
        { "class": "filetype-zip", "protocol": "WWW:DOWNLOAD-1.0-http--download", "type": "application/zip" },
        { "class": "filetype-wms", "protocol": "OGC:WMS" },
        { "class": "filetype-png", "protocol": "WWW:DOWNLOAD-1.0-http--download", "type": "image/png" },
        { "class": "filetype-pdf", "protocol": "WWW:DOWNLOAD-1.0-http--download", "type": "application/pdf" },
        { "class": "filetype-jpg", "protocol": "WWW:DOWNLOAD-1.0-http--download", "type": "image/jpeg" },
        { "class": "filetype-rest", "protocol": "REST" },
        { "class": "filetype-kml", "protocol": "GLG:KML-2.0-http-get-map" }
    ];

    /*
     * Maps mimetypes to image files and alternative text.
     */
    self.mimeTypes = [
        {
            type: 'application/zip',
            image: '../../Images/filetype_icons/small/zip.png',
            largeImage: '../../Images/filetype_icons/large/zip.png',
            alt: 'Archive'
        },
        {
            type: 'image/png',
            image: '../../Images/filetype_icons/small/png.png',
            largeImage: '../../Images/filetype_icons/large/png.png',
            alt: 'PNG Image'
        },
        {
            type: 'image/jpeg',
            image: '../../Images/filetype_icons/small/jpeg.png',
            largeImage: '../../Images/filetype_icons/large/jpeg.png',
            alt: 'JPEG Image'
        },
        {
            type: 'image/jpg',
            image: '../../Images/filetype_icons/small/jpeg.png',
            largeImage: '../../Images/filetype_icons/large/jpeg.png',
            alt: 'JPEG Image'
        },
        {
            type: 'application/vnd.ogc.wms_xml',
            image: '../../Images/filetype_icons/small/wms.png',
            largeImage: '../../Images/filetype_icons/large/wms.png',
            alt: 'WMS Download'
        },
        {
            type: 'application/vnd.google-earth.kml+xml',
            image: '../../Images/filetype_icons/small/kml.png',
            largeImage: '../../Images/filetype_icons/large/kml.png',
            alt: 'KML'
        },
        {
            type: 'text/plain',
            image: '../../Images/filetype_icons/small/plain.png',
            largeImage: '../../Images/filetype_icons/large/plain.png',
            alt: 'Plain Text'
        },
        {
            type: 'text/html',
            image: '../../Images/filetype_icons/small/html.png',
            largeImage: '../../Images/filetype_icons/large/html.png',
            alt: 'HTML'
        }, {
            type: 'application/pdf',
            image: '../../Images/filetype_icons/small/pdf.png',
            largeImage: '../../Images/filetype_icons/large/pdf.png',
            alt: 'PDF Document'
        }
    ];

    /*
     * Maps protocols to image files and alternate names.
     * There is currently only one protocol that falls back onto the mimetype
     * mappings, the download protocol.
     */
    self.iconLookup = [
        { 
            protocol: 'OGC:WMS', 
            image: '../../Images/filetype_icons/small/wms.png',
            largeImage: '../../Images/filetype_icons/large/wms.png',
            alt: 'WMS' },
        { 
            protocol: 'GLG:KML-2.0-http-get-map', 
            image: '../../Images/filetype_icons/small/kml.png',
            largeImage: '../../Images/filetype_icons/large/kml.png',
            alt: 'Google Earth' },
        { 
            protocol: 'REST', 
            image: '../../Images/filetype_icons/small/rest.png',
            largeImage: '../../Images/filetype_icons/large/rest.png',
            alt: 'ESRI API' },
        { 
            protocol: 'OGC:WMS-1.1.1-http-get-map', 
            image: '../../Images/filetype_icons/small/wms.png',
            largeImage: '../../Images/filetype_icons/large/wms.png',
            alt: 'WMS' },
        { 
            protocol: 'WWW:LINK-1.0-http--link', 
            image: '../../Images/filetype_icons/small/link.png',
            largeImage: '../../Images/filetype_icons/large/link.png',
            alt: 'Link' },
        { 
            protocol: 'WWW:DOWNLOAD-1.0-http--download', 
            mimetypes: self.mimeTypes },
    ];

    self.unknownFiletype = "../../Images/filetype_icons/large/_blank.png";

    function findFiletypeContent(protocol, mimetype) {
        var results = {};
        $(self.iconLookup).each(function (index, prot) {
            if (prot !== undefined && prot.protocol == protocol) {
                if (prot.mimetypes) {
                    $(prot.mimetypes).each(function (index, mime) {
                        if (mime.type == mimetype) {
                            results.image = mime.image;
                            results.largeImage = mime.largeImage;
                            results.alt = mime.alt;
                            return false;
                        }
                    });
                    return false;
                } else {
                    results.image = prot.image;
                    results.largeImage = prot.largeImage;
                    results.alt = prot.alt;
                    return false;
                }
            }
        });
        return results;
    }

    /*
     * Used to create the large file-type icon used in the metadata detail view.
     */
    self.createLargeImageTag = function (parent) {
        $.each(this.filetypeMappings, function (index, data) {
            var cls = data['class'];
            if ($(parent).hasClass(cls)) {
                var details = findFiletypeContent(data.protocol, data.type);
                var image = details.largeImage;
                $(parent).append($("<img src='" + image + "' />"));
                return false;
            }
        });
        if ($(parent).children().length == 0) {
            var image = self.unknownFiletype;
            $(parent).append($("<img src='" + image + "' />"));
        }
    }

    /*
     * Determines the human readable name for the given metadata name.  Provided name is first 
     * checked against the mimetype list, then against the protocols.
     */
    self.resourceFormatMapper = function (name) {
        var fancyName = '';
        $(self.mimeTypes).each(function (index, mimetype) {
            if (mimetype.type == name) {
                fancyName = mimetype.alt;
                return false;
            }
        });
        if (fancyName == '') {
            $(self.iconLookup).each(function (index, protocol) {
                if (protocol !== undefined && protocol.protocol == name) {
                    fancyName = protocol.alt;
                    return false;
                }
            });
        }
        return fancyName != '' ? fancyName : name;
    }

    /*
     * Used to create the small icons used by the search results page.
     */
    
    self.createSmallFiletypeIcon = function (link) {
        var bits = link.split("|");
        var protocol = bits[3];
        var mimetype = bits[4];
        var details = findFiletypeContent(protocol, mimetype);

        if (details.image !== undefined) {
            var link = $('<a href="#">');
            var result = $('<img>').attr('src', details.image).attr('alt', details.alt);
            result = $(link).append(result);
            $(result).click(function (e) {
                var resource = new Dataset(bits[5], /\/*(.+)\..*/.exec(bits[2])[1], details.image, [new DatasetItem(/(?:\.([^.]+))?$/.exec(bits[2])[1], bits[2], "128")]);
                cart.packageAndFetch(resource);
            });
            return result;
        }
        return null;
    } 
}
