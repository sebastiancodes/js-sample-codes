﻿/*
 * The truncation is done as follows, for a given maximum, find the first newline before that limit. If none are 
 * found, find the last tab or space character before that limit, and within a given search limit.  If none are 
 * found there, find the last non-alphabetic character within a search limit.  Finally, if no break point is 
 * found yet, chop at the limit.  We then add an ellipsis, so the actual maximum
 * length that may be returned is five characters more than the stated limit.
 */
function TruncateString(fullBody, charLength, searchWidth) {
    if (fullBody === undefined) {
        return;
    }
    if (charLength == null) {
        charLength = 1000;
    }
    if (searchWidth == null) {
        searchWidth = 100;
    }

    var index = -1;

    var shortBody = fullBody.substring(0, charLength);
    var index = shortBody.indexOf("\n");

    if (index == -1) {
        var i = shortBody.lastIndexOf(" ");
        var j = shortBody.lastIndexOf("\t");
        var k = (i > j) ? i : j;
        if (k > charLength - searchWidth) {
            index = k;
        }
    }
    if (index == -1) {
        for (var i = charLength; i > charLength - searchWidth; i--) {
            var char = shortBody.charAt(i);
            if (char < "A" || char > "z") {
                index = i;
                break;
            }
        }
    }
    if (index != -1) {
        shortBody = fullBody.substring(0, index);
    }

    return shortBody;
}

function PrintableMonth(monthNum) {
    var lookup = [
        "January", "February", "March",
        "April", "May", "June",
        "July", "August", "September",
        "October", "November", "December"
    ];
    return lookup[monthNum];
}

function PrintableQuarter(quarterNum, year) {
    var lookup = [
        "Q1", "Q2", "Q3", "Q4"
    ];
    return lookup[quarterNum] + " " + year;
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g,
        function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
}

if (!Date.prototype.toISOString) {
    console.log("added toISOString");
    (function () {
        function pad(number) {
            var r = String(number);
            if (r.length === 1) {
                r = '0' + r;
            }
            return r;
        }
        Date.prototype.toISOString = function () {
            return this.getUTCFullYear()
                + '-' + pad(this.getUTCMonth() + 1)
                + '-' + pad(this.getUTCDate())
                + 'T' + pad(this.getUTCHours())
                + ':' + pad(this.getUTCMinutes())
                + ':' + pad(this.getUTCSeconds())
                + '.' + String((this.getUTCMilliseconds() / 1000).toFixed(3)).slice(2, 5)
                + 'Z';
        };
    }());
}